# Rozproszone Projekt PC #
Aplikacja wspomagająca działanie sklepu muzycznego. Pozwala min. na definiowanie albumów, ich wykonawców oraz piosenek, a także lokalizacji w sklepie.

Praca zaliczeniowa z przedmiotu "Systemy rozproszone" - Czerwiec 2016.

Cały system składał się z 3 aplikacji: na PC, na smartfony (Android) oraz na tablety (Android). Całość wykonywana była w zespole 3-osobowym, ja byłem odpowiedzialny za aplikację na PC.