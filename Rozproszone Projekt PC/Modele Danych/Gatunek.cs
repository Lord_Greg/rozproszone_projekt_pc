﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rozproszone_Projekt_PC.Obsługa_BD;
using System.Windows.Forms;

namespace Rozproszone_Projekt_PC.Modele_Danych
{
    class Gatunek
    {
        public string nazwa;

        public string Nazwa
        {
            get { return nazwa; }
        }

        public Gatunek(string nazwa)
        {
            this.nazwa = nazwa;
        }

        public void dodajNowyDoBazy()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;
                polecenie = Polecenia_SQL.wstawWartości(Obsługa_BD.Opisy.tab_Gatunki, new string[] { nazwa });

                łącznik.wykonajPolecenieBezzwrotne(polecenie);
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd zapisu do bazy:\n" + e.Message);
            }
        }

        public static List<Gatunek> wczytajWszystkoZbazy()
        {
            List<Gatunek> lista = new List<Gatunek>();

            Łączka łącznik = new Łączka();
            string polecenie;
            polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Gatunki);

            łącznik.połącz();
            MySql.Data.MySqlClient.MySqlDataReader kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

            while(kursor.Read())
            {
                lista.Add(new Gatunek(kursor.GetString(0)));
            }
            łącznik.rozłącz();

            return lista;
        }



        public void usuńZbazy()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Gatunki_Albumów, new string[] { Opisy.gatunki_albumów_nazwa_gatunku }, new string[] { nazwa });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);
                
                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Gatunki, new string[] { Opisy.gatunki_nazwa }, new string[] { nazwa });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd usuwania danych:\n" + e.Message);
            }
        }


        public void aktualizujWbazie(string nowaNazwa)
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;
                string warunek;
                List<string> idAlbumów = new List<string>();

                //zapamiętywanie wartoścci z hasza
                warunek = Opisy.gatunki_albumów_nazwa_gatunku + "=\"" + nazwa + "\"";
                polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Gatunki_Albumów, warunek);

                łącznik.połącz();
                MySql.Data.MySqlClient.MySqlDataReader kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                while(kursor.Read())
                {
                    idAlbumów.Add(kursor.GetString(1));
                }
                kursor.Close();
                łącznik.rozłącz();

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Gatunki_Albumów, new string[] { Opisy.gatunki_albumów_nazwa_gatunku }, new string[] { nazwa });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);


                //aktualizacja właściwa
                warunek = Opisy.gatunki_nazwa + "=\"" + nazwa + "\"";
                polecenie = Polecenia_SQL.aktualizujNPól(Opisy.tab_Gatunki, new string[] { Opisy.gatunki_nazwa }, new string[] { nowaNazwa }, warunek);
                łącznik.wykonajPolecenieBezzwrotne(polecenie);


                //przywracanie hasza
                foreach (string idAlbumu in idAlbumów)
                {
                    polecenie = Polecenia_SQL.wstawWartości(Opisy.tab_Gatunki_Albumów, new string[] { nowaNazwa, idAlbumu });
                    łącznik.wykonajPolecenieBezzwrotne(polecenie);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd zapisu do bazy:\n" + e.Message);
            }
        }
    }
}
