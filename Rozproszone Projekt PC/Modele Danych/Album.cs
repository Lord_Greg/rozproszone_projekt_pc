﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rozproszone_Projekt_PC.Obsługa_BD;

namespace Rozproszone_Projekt_PC.Modele_Danych
{
    class Album
    {
        public string nazwa, kodKreskowy;
        public double cena;
        public int id, rokWydania, idWydawcy, ilośćNaStanie, ilośćSprzedanych;
        public List<Ścieżka> listaŚcieżek;
        public List<Gatunek> listaGatunków;
        public List<Artysta> listaArtystów;
        public List<Zespół> listaZespołów;
        public List<Lokalizacja> listaLokalizacji;

        public string Id
        {
            get
            {
                return id.ToString();
            }
        }

        public string Nazwa
        {
            get
            {
                return nazwa;
            }
        }

        public string NazwaZwykonawcą
        {
            get
            {
                string tekstDoZwrotu = "";
                tekstDoZwrotu += nazwa;

                if(listaArtystów.Count>0 || listaZespołów.Count>0)
                {
                    tekstDoZwrotu += "(";
                    foreach(Zespół zespół in listaZespołów)
                    {
                        tekstDoZwrotu += zespół.nazwa + ", ";
                    }
                    foreach (Artysta artysta in listaArtystów)
                    {
                        tekstDoZwrotu += artysta.Nazwa + ", ";
                    }

                    StringBuilder strbTekstZwrotny = new StringBuilder();
                    strbTekstZwrotny.Append(tekstDoZwrotu, 0, tekstDoZwrotu.Length - 2);
                    tekstDoZwrotu = strbTekstZwrotny.ToString();

                    tekstDoZwrotu += ")";
                }

                return tekstDoZwrotu;
            }
        }

        public string IdWydawcy
        {
            get
            {
                string tekstDoZwrotu = "";

                if (idWydawcy > 0) tekstDoZwrotu = idWydawcy.ToString();
                else tekstDoZwrotu = "null";

                return tekstDoZwrotu;
            }
        }


        public Album(int idAlbumu, string nazwa)
        {
            this.id = idAlbumu;
            this.nazwa = nazwa;
            this.rokWydania = 2000;
            this.idWydawcy = 0;
            this.cena = 0;
            this.ilośćNaStanie = 0;
            this.ilośćSprzedanych = 0;
            this.kodKreskowy = "";
            this.listaŚcieżek = new List<Ścieżka>();
            this.listaGatunków = new List<Gatunek>();
            this.listaArtystów = new List<Artysta>();
            this.listaZespołów = new List<Zespół>();
            this.listaLokalizacji = new List<Lokalizacja>();
        }

        public Album(string[] tablicaDanychZbazy)
        {
            Int32.TryParse(tablicaDanychZbazy[0], out this.id);
            this.nazwa = tablicaDanychZbazy[1];
            Int32.TryParse(tablicaDanychZbazy[2], out this.rokWydania);
            Int32.TryParse(tablicaDanychZbazy[3], out this.idWydawcy);
            Double.TryParse(tablicaDanychZbazy[4], out this.cena);
            Int32.TryParse(tablicaDanychZbazy[5], out this.ilośćNaStanie);
            Int32.TryParse(tablicaDanychZbazy[6], out this.ilośćSprzedanych);
            this.kodKreskowy = tablicaDanychZbazy[7];
            this.listaŚcieżek = new List<Ścieżka>();
            this.listaGatunków = new List<Gatunek>();
            this.listaArtystów = new List<Artysta>();
            this.listaZespołów = new List<Zespół>();
            this.listaLokalizacji = new List<Lokalizacja>();
        }

        public Album(string nazwa, int rokWydania, int idWydawcy, double cena, int ilośćNaStanie, int ilośćSprzedanych, string kodKreskowy)
        {
            this.nazwa = nazwa;
            this.rokWydania = rokWydania;
            this.idWydawcy = idWydawcy;
            this.cena = cena;
            this.ilośćNaStanie = ilośćNaStanie;
            this.ilośćSprzedanych = ilośćSprzedanych;
            this.kodKreskowy = kodKreskowy;
        }

        public Album(string nazwa, int rokWydania, int idWydawcy, double cena, int ilośćNaStanie, int ilośćSprzedanych, string kodKreskowy, List<Artysta> listaArtystów, List<Zespół> listaZespołów, List<Ścieżka> listaŚcieżek, List<Gatunek> listaGatunków, List<Lokalizacja> listaLokalizacji)
        {
            this.nazwa = nazwa;
            this.rokWydania = rokWydania;
            this.idWydawcy = idWydawcy;
            this.cena = cena;
            this.ilośćNaStanie = ilośćNaStanie;
            this.ilośćSprzedanych = ilośćSprzedanych;
            this.kodKreskowy = kodKreskowy;
            this.listaŚcieżek = new List<Ścieżka>(listaŚcieżek);
            this.listaGatunków = new List<Gatunek>(listaGatunków);
            this.listaArtystów = new List<Artysta>(listaArtystów);
            this.listaZespołów = new List<Zespół>(listaZespołów);
            this.listaLokalizacji = new List<Lokalizacja>(listaLokalizacji);
        }

        public Album(int idAlbumu, string nazwa, int rokWydania, int idWydawcy, double cena, int ilośćNaStanie, int ilośćSprzedanych, string kodKreskowy, List<Artysta> listaArtystów, List<Zespół> listaZespołów, List<Ścieżka> listaŚcieżek, List<Gatunek> listaGatunków, List<Lokalizacja> listaLokalizacji)
        {
            this.id = idAlbumu;
            this.nazwa = nazwa;
            this.rokWydania = rokWydania;
            this.idWydawcy = idWydawcy;
            this.cena = cena;
            this.ilośćNaStanie = ilośćNaStanie;
            this.ilośćSprzedanych = ilośćSprzedanych;
            this.kodKreskowy = kodKreskowy;
            this.listaŚcieżek = new List<Ścieżka>(listaŚcieżek);
            this.listaGatunków = new List<Gatunek>(listaGatunków);
            this.listaArtystów = new List<Artysta>(listaArtystów);
            this.listaZespołów = new List<Zespół>(listaZespołów);
            this.listaLokalizacji = new List<Lokalizacja>(listaLokalizacji);
        }



        public void dodajNowyDoBazy()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie = "";

                polecenie += Obsługa_BD.Polecenia_SQL.wstawWartości_1z3(Obsługa_BD.Opisy.tab_Albumy);
                polecenie += Obsługa_BD.Polecenia_SQL.wstawWartości_2z3_Zwykłe(new string[] { "", nazwa, rokWydania.ToString() }, true);
                polecenie += Obsługa_BD.Polecenia_SQL.wstawWartości_2z3_Specjalne(new string[] { idWydawcy.ToString() }, false);
                polecenie += Obsługa_BD.Polecenia_SQL.wstawWartości_2z3_Zwykłe(new string[] { zamieńPrzecinekNaKropke(String.Format("{0:0.00}", cena)), ilośćNaStanie.ToString(), ilośćSprzedanych.ToString(), kodKreskowy }, false);
                polecenie += Obsługa_BD.Polecenia_SQL.wstawWartości_3z3();

                //polecenie = Obsługa_BD.Polecenia_SQL.wstawWartości(Obsługa_BD.Opisy.tab_Albumy, new string[] { "", nazwa, rokWydania.ToString(), idWydawcy.ToString(), String.Format("{0:0.00}", cena), ilośćNaStanie.ToString(), ilośćSprzedanych.ToString(), kodKreskowy });

                łącznik.wykonajPolecenieBezzwrotne(polecenie);


                polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Albumy, Opisy.albumy_nazwa + '=' + '"' + nazwa + '"' + " ORDER BY " + Opisy.albumy_id_albumu + " ASC");
                łącznik.połącz();

                MySql.Data.MySqlClient.MySqlDataReader kursor = łącznik.wykonajPolecenieZwrotne(polecenie);
                kursor.Read();
                id = kursor.GetInt32(0);
                kursor.Close();
                łącznik.rozłącz();

                //dodawanie danych pochodnych
                //Piosenki
                foreach (Ścieżka ścieżka in listaŚcieżek)
                {
                    try
                    {
                        polecenie = Polecenia_SQL.wstawWartości(Opisy.tab_Piosenki_w_Albumach, new string[] { id.ToString(), ścieżka.piosenka.id.ToString(), ścieżka.nrŚcieżki.ToString() });
                        łącznik.wykonajPolecenieBezzwrotne(polecenie);
                    }
                    catch(Exception e)
                    {
                        //MessageBox.Show("Wystąpił błąd przy zapisie ścieżek: " + e.Message);
                    }
                }

                //Gatunki
                foreach (Gatunek gatunek in listaGatunków)
                {
                    try
                    {
                        polecenie = Polecenia_SQL.wstawWartości(Opisy.tab_Gatunki_Albumów, new string[] { gatunek.nazwa, id.ToString() });
                        łącznik.wykonajPolecenieBezzwrotne(polecenie);
                    }
                    catch (Exception e)
                    {
                        //MessageBox.Show("Wystąpił błąd przy zapisie gatunków: " + e.Message);
                    }
                }

                //Zespoły
                foreach (Zespół zespół in listaZespołów)
                {
                    try
                    {
                        polecenie = Polecenia_SQL.wstawWartości(Opisy.tab_Zespoły_w_Albumach, new string[] { zespół.id.ToString(), id.ToString() });
                        łącznik.wykonajPolecenieBezzwrotne(polecenie);
                    }
                    catch (Exception e)
                    {
                        //MessageBox.Show("Wystąpił błąd przy zapisie wykonawców: " + e.Message);
                    }
                }

                //Artyści
                foreach (Artysta artysta in listaArtystów)
                {
                    try
                    {
                        polecenie = Polecenia_SQL.wstawWartości(Opisy.tab_Artyści_w_Albumach, new string[] { artysta.id.ToString(), id.ToString() });
                        łącznik.wykonajPolecenieBezzwrotne(polecenie);
                    }
                    catch (Exception e)
                    {
                        //MessageBox.Show("Wystąpił błąd przy zapisie wykonawców: " + e.Message);
                    }
                }

                //Lokalizacje
                foreach (Lokalizacja lokalizacja in listaLokalizacji)
                {
                    try
                    {
                        polecenie = Polecenia_SQL.wstawWartości(Opisy.tab_Lokalizacje_Albumów, new string[] { lokalizacja.id.ToString(), id.ToString() });
                        łącznik.wykonajPolecenieBezzwrotne(polecenie);
                    }
                    catch (Exception e)
                    {
                        //MessageBox.Show("Wystąpił błąd przy zapisie lokalizacji: " + e.Message);
                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd zapisu do bazy:\n" + e.Message);
            }

        }

        public static List<Album> wczytajWszystkie()
        {
            List<Album> listaAlbumów = new List<Album>();

            try
            {
                Łączka łącznik = new Łączka();
                łącznik.połącz();

                string polecenie;

                polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Albumy);

                MySql.Data.MySqlClient.MySqlDataReader kursor;

                kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                while (kursor.Read())
                {
                    string[] tabDanych = new string[kursor.FieldCount];
                    for (int i = 0; i < kursor.FieldCount; i++)
                    {
                        try { tabDanych[i] = kursor.GetString(i); }
                        catch(Exception e) { tabDanych[i] = ""; }
                    }

                    listaAlbumów.Add(new Album(tabDanych));
                }
                kursor.Close();

                string warunek;

                foreach (Album album in listaAlbumów)
                {
                    //Zespoły
                    warunek = 
                        Opisy.tab_Zespoły_w_Albumach + "." + Opisy.zespoły_w_albumach_id_albumu + "=" + album.Id
                        + " AND " +
                        Opisy.tab_Zespoły_w_Albumach + "." + Opisy.zespoły_w_albumach_id_zespołu + "=" + Opisy.tab_Zespoły + "." + Opisy.zespoły_id_zespołu;

                    polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Zespoły + ", " + Opisy.tab_Zespoły_w_Albumach, warunek);

                    kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                    while (kursor.Read())
                    {
                        album.listaZespołów.Add(new Zespół(kursor.GetInt32(0), kursor.GetString(1), kursor.GetInt32(2), kursor.GetString(3)));
                    }
                    kursor.Close();


                    //Artyści
                    warunek =
                        Opisy.tab_Artyści_w_Albumach + "." + Opisy.artyści_w_albumach_id_albumu + "=" + album.Id
                        + " AND " +
                        Opisy.tab_Artyści_w_Albumach + "." + Opisy.artyści_w_albumach_id_artysty + "=" + Opisy.tab_Artyści + "." + Opisy.artyści_id_artysty;

                    polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Artyści + ", " + Opisy.tab_Artyści_w_Albumach, warunek);

                    kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                    while (kursor.Read())
                    {
                        album.listaArtystów.Add(new Artysta(kursor.GetInt32(0), kursor.GetString(1), kursor.GetString(2), kursor.GetString(3), kursor.GetString(4)));
                    }
                    kursor.Close();


                    //Gatunki
                    warunek =
                        Opisy.tab_Gatunki_Albumów + "." + Opisy.gatunki_albumów_id_albumu + "=" + album.Id
                        + " AND " +
                        Opisy.tab_Gatunki_Albumów + "." + Opisy.gatunki_albumów_nazwa_gatunku + "=" + Opisy.tab_Gatunki + "." + Opisy.gatunki_nazwa;

                    polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Gatunki + ", " + Opisy.tab_Gatunki_Albumów, warunek);

                    kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                    while (kursor.Read())
                    {
                        album.listaGatunków.Add(new Gatunek(kursor.GetString(0)));
                    }
                    kursor.Close();



                    //Piosenki
                    warunek = Opisy.tab_Piosenki_w_Albumach + "." + Opisy.piosenki_w_albumach_id_albumu + "=" + album.Id
                        + " AND " +
                        Opisy.tab_Piosenki_w_Albumach + "." + Opisy.piosenki_w_albumach_id_piosenki + "=" + Opisy.tab_Piosenki + "." + Opisy.piosenki_id_piosenki;

                    polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Piosenki + ", " + Opisy.tab_Piosenki_w_Albumach, warunek);

                    kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                    while (kursor.Read())
                    {
                        string idZespołu, idArtysty;
                        try { idZespołu = kursor.GetString(3); }
                        catch(Exception e) { idZespołu = "0"; }

                        try { idArtysty = kursor.GetString(4); }
                        catch (Exception e) { idArtysty = "0"; }

                        album.listaŚcieżek.Add(new Ścieżka(kursor.GetInt32(7), new Piosenka(kursor.GetString(0), kursor.GetString(1), kursor.GetString(2), idZespołu, idArtysty)));
                    }
                    kursor.Close();



                    //Lokalizacje
                    warunek = Opisy.tab_Lokalizacje_Albumów + "." + Opisy.lokalizacje_albumów_id_albumu + "=" + album.Id
                    + " AND " +
                    Opisy.tab_Lokalizacje_Albumów + "." + Opisy.lokalizacje_albumów_id_lokalizacji + "=" + Opisy.tab_Lokalizacje + "." + Opisy.lokalizacje_id_lokalizacji;

                    polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Lokalizacje + ", " + Opisy.tab_Lokalizacje_Albumów, warunek);

                    kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                    while (kursor.Read())
                    {
                        album.listaLokalizacji.Add(new Lokalizacja(kursor.GetInt32(0), kursor.GetString(1), kursor.GetString(2)));
                    }
                    kursor.Close();




                }

                łącznik.rozłącz();
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd wczytywania danych:\n" + e.Message);
            }

            return listaAlbumów;
        }



        public void usuńZbazy()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Piosenki_w_Albumach, new string[] { Opisy.piosenki_w_albumach_id_albumu }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Zespoły_w_Albumach, new string[] { Opisy.zespoły_w_albumach_id_albumu }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Artyści_w_Albumach, new string[] { Opisy.artyści_w_albumach_id_albumu }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Lokalizacje_Albumów, new string[] { Opisy.lokalizacje_albumów_id_albumu }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Gatunki_Albumów, new string[] { Opisy.gatunki_albumów_id_albumu }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);



                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Albumy, new string[] { Opisy.albumy_id_albumu }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd usuwania danych:\n" + e.Message);
            }
        }


        public void aktualizujWbazie()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie = "";
                string warunek;

                warunek = Opisy.albumy_id_albumu + "=" + Id;

                //Piosenki
                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Piosenki_w_Albumach, new string[] { Opisy.piosenki_w_albumach_id_albumu }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                foreach (Ścieżka ścieżka in listaŚcieżek)
                {
                    try
                    {
                        polecenie = Polecenia_SQL.wstawWartości(Opisy.tab_Piosenki_w_Albumach, new string[] { id.ToString(), ścieżka.piosenka.id.ToString(), ścieżka.nrŚcieżki.ToString() });
                        łącznik.wykonajPolecenieBezzwrotne(polecenie);
                    }
                    catch (Exception e)
                    {
                        //MessageBox.Show("Wystąpił błąd przy zapisie ścieżek: " + e.Message);
                    }
                }

                //Zespoły
                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Zespoły_w_Albumach, new string[] { Opisy.zespoły_w_albumach_id_albumu }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                foreach (Zespół zespół in listaZespołów)
                {
                    try
                    {
                        polecenie = Polecenia_SQL.wstawWartości(Opisy.tab_Zespoły_w_Albumach, new string[] { zespół.id.ToString(), id.ToString() });
                        łącznik.wykonajPolecenieBezzwrotne(polecenie);
                    }
                    catch (Exception e)
                    {
                        //MessageBox.Show("Wystąpił błąd przy zapisie wykonawców: " + e.Message);
                    }
                }

                //Artyści
                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Artyści_w_Albumach, new string[] { Opisy.artyści_w_albumach_id_albumu }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                foreach (Artysta artysta in listaArtystów)
                {
                    try
                    {
                        polecenie = Polecenia_SQL.wstawWartości(Opisy.tab_Artyści_w_Albumach, new string[] { artysta.id.ToString(), id.ToString() });
                        łącznik.wykonajPolecenieBezzwrotne(polecenie);
                    }
                    catch (Exception e)
                    {
                        //MessageBox.Show("Wystąpił błąd przy zapisie wykonawców: " + e.Message);
                    }
                }

                //Lokalizacje
                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Lokalizacje_Albumów, new string[] { Opisy.lokalizacje_albumów_id_albumu }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                foreach (Lokalizacja lokalizacja in listaLokalizacji)
                {
                    try {
                    polecenie = Polecenia_SQL.wstawWartości(Opisy.tab_Lokalizacje_Albumów, new string[] { lokalizacja.id.ToString(), id.ToString() });
                    łącznik.wykonajPolecenieBezzwrotne(polecenie);
                    }
                    catch (Exception e)
                    {
                        //MessageBox.Show("Wystąpił błąd przy zapisie lokalizacji: " + e.Message);
                    }
                }

                //Gatunki
                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Gatunki_Albumów, new string[] { Opisy.gatunki_albumów_id_albumu }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                foreach (Gatunek gatunek in listaGatunków)
                {
                    try
                    {
                        polecenie = Polecenia_SQL.wstawWartości(Opisy.tab_Gatunki_Albumów, new string[] { gatunek.nazwa, id.ToString() });
                        łącznik.wykonajPolecenieBezzwrotne(polecenie);
                    }
                    catch (Exception e)
                    {
                        //MessageBox.Show("Wystąpił błąd przy zapisie gatunków: " + e.Message);
                    }
                }



                polecenie = Polecenia_SQL.aktualizujNPól(Opisy.tab_Albumy, new string[] { Opisy.albumy_nazwa, Opisy.albumy_rok_wydania, Opisy.albumy_cena, Opisy.albumy_ilość_na_stanie, Opisy.albumy_ilość_sprzedanych, Opisy.albumy_kod_kreskowy }, new string[] { nazwa, rokWydania.ToString(), cena.ToString(), ilośćNaStanie.ToString(), ilośćSprzedanych.ToString(), kodKreskowy }, warunek);
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                polecenie = Polecenia_SQL.aktualizujNPól_Specjalne(Opisy.tab_Albumy, new string[] { Opisy.albumy_id_wydawcy }, new string[] { IdWydawcy }, warunek);
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd zapisu do bazy:\n" + e.Message);
            }
        }


        private string zamieńPrzecinekNaKropke(string tekst)
        {
            string zamienionyTekst = "";

            for (int i = 0; i < tekst.Length; i++)
            {
                if (tekst[i] == ',') zamienionyTekst += '.';
                else zamienionyTekst += tekst[i];
            }
            return zamienionyTekst;
        }
    }

    class Ścieżka
    {
        public int nrŚcieżki;
        public Piosenka piosenka;

        public Ścieżka(int nrŚcieżki, Piosenka piosenka)
        {
            this.nrŚcieżki = nrŚcieżki;
            this.piosenka = piosenka;
        }
    }
}