﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rozproszone_Projekt_PC.Obsługa_BD;

namespace Rozproszone_Projekt_PC.Modele_Danych
{
    class Piosenka
    {
        public string id, tytuł, idZespołu, idArtysty;
        public Długość_Piosenki długość;

        public string Tytuł
        {
            get { return tytuł; }
        }

        public string Id
        {
            get { return id; }
        }

        public string IdZespołu
        {
            get
            {
                string tekstDoZwrotu = idZespołu;
                if(tekstDoZwrotu == "" || tekstDoZwrotu == "-1" || tekstDoZwrotu == "0")
                {
                    tekstDoZwrotu = "null";
                }
                return tekstDoZwrotu;
            }
        }

        public string IdArtysty
        {
            get
            {
                string tekstDoZwrotu = idArtysty;
                if (tekstDoZwrotu == "" || tekstDoZwrotu == "-1" || tekstDoZwrotu == "0")
                {
                    tekstDoZwrotu = "null";
                }
                return tekstDoZwrotu;
            }
        }

        /*public Piosenka(string id, string tytuł, string długość, string idZespołu, string idArtysty, bool nieIstotnaZmienna)
        {
            this.id = id;
            this.tytuł = tytuł;
            this.długość = new Długość_Piosenki(długość);
            this.idZespołu = idZespołu;
            this.idArtysty = idArtysty;
        }*/
        public Piosenka()
        {
            this.id = "-1";
            this.tytuł = "";
            this.długość = new Długość_Piosenki(0);
            this.idZespołu = "";
            this.idArtysty = "";
        }

        public Piosenka(int idPiosenki)
        {
            this.id = idPiosenki.ToString();
            this.tytuł = "";
            this.długość = new Długość_Piosenki(0);
            this.idZespołu = "";
            this.idArtysty = "";
        }

        public Piosenka(string[] tablicaDanychZbazy)
        {
            this.id = tablicaDanychZbazy[0];
            this.tytuł = tablicaDanychZbazy[1];
            this.długość = new Długość_Piosenki(tablicaDanychZbazy[2]);

            if (tablicaDanychZbazy[3] != "") this.idZespołu = tablicaDanychZbazy[3];
            else this.idZespołu = "0";

            if (tablicaDanychZbazy[4] != "") this.idArtysty = tablicaDanychZbazy[4];
            else this.idArtysty = "0";
        }

        public Piosenka(string id, string tytuł, string długość, string idZespołu, string idArtysty)
        {
            this.id = id;
            this.tytuł = tytuł;
            this.długość = new Długość_Piosenki(długość);

            if (idZespołu != "") this.idZespołu = idZespołu;
            else this.idZespołu = "0";

            if (idArtysty != "") this.idArtysty = idArtysty;
            else this.idArtysty = "0";
        }

        public Piosenka(string id, string tytuł, string długośćGodziny, string długośćMinuty, string długośćSekundy, string idZespołu, string idArtysty)
        {
            this.id = id;
            this.tytuł = tytuł;
            this.długość = new Długość_Piosenki(Convert.ToInt32(długośćGodziny), Convert.ToInt32(długośćMinuty), Convert.ToInt32(długośćSekundy));
            this.idZespołu = idZespołu;
            this.idArtysty = idArtysty;
        }
        
        public Piosenka(string tytuł, string długośćGodziny, string długośćMinuty, string długośćSekundy, string idZespołu, string idArtysty)
        {
            this.tytuł = tytuł;
            this.długość = new Długość_Piosenki(Convert.ToInt32(długośćGodziny), Convert.ToInt32(długośćMinuty), Convert.ToInt32(długośćSekundy));
            this.idZespołu = idZespołu;
            this.idArtysty = idArtysty;
        }
        
        public Piosenka(string tytuł, int długośćGodziny, int długośćMinuty, int długośćSekundy, string idZespołu, string idArtysty)
        {
            this.tytuł = tytuł;
            this.długość = new Długość_Piosenki(długośćGodziny, długośćMinuty, długośćSekundy);
            this.idZespołu = idZespołu;
            this.idArtysty = idArtysty;
        }


        public void dodajNowyDoBazy()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie = "";
                polecenie += Obsługa_BD.Polecenia_SQL.wstawWartości_1z3(Obsługa_BD.Opisy.tab_Piosenki);
                polecenie += Obsługa_BD.Polecenia_SQL.wstawWartości_2z3_Zwykłe( new string[] { "", tytuł, długość.Długość } , true);
                if(idZespołu=="")
                    polecenie += Obsługa_BD.Polecenia_SQL.wstawWartości_2z3_Specjalne(new string[] { idZespołu }, false);
                else
                    polecenie += Obsługa_BD.Polecenia_SQL.wstawWartości_2z3_Zwykłe(new string[] { idZespołu }, false);

                if (idArtysty == "")
                    polecenie += Obsługa_BD.Polecenia_SQL.wstawWartości_2z3_Specjalne(new string[] { idArtysty }, false);
                else
                    polecenie += Obsługa_BD.Polecenia_SQL.wstawWartości_2z3_Zwykłe(new string[] { idArtysty }, false);

                polecenie += Obsługa_BD.Polecenia_SQL.wstawWartości_3z3();

                //polecenie = Polecenia_SQL.wstawWartości(Obsługa_BD.Opisy.tab_Piosenki, new string[] { "", tytuł, długość.Długość, idZespołu, idArtysty } );

                łącznik.wykonajPolecenieBezzwrotne(polecenie);
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd zapisu do bazy:\n" + e.Message);
            }
        }



        public static List<Piosenka> wczytajZbazy(string warunek)
        {
            List<Piosenka> listaPiosenek = new List<Piosenka>();

            try
            {
                Łączka łącznik = new Łączka();
                łącznik.połącz();

                string polecenie;

                polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Piosenki, warunek);

                MySql.Data.MySqlClient.MySqlDataReader kursor;

                kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                while (kursor.Read())
                {
                    string[] tabDanych = new string[kursor.FieldCount];
                    for(int i=0; i<kursor.FieldCount; i++)
                    {
                        try { tabDanych[i] = kursor.GetString(i); }
                        catch { tabDanych[i] = ""; }
                    }

                    listaPiosenek.Add(new Piosenka(tabDanych));

                    //listaPiosenek.Add(new Piosenka(kursor.GetString(0), kursor.GetString(1), kursor.GetString(2), kursor.GetString(3), kursor.GetString(4), true));
                }

                łącznik.rozłącz();
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd wczytywania danych:\n" + e.Message);
            }

            return listaPiosenek;
        }


        public static List<Piosenka> wczytajWszystkie()
        {
            List<Piosenka> listaPiosenek = new List<Piosenka>();

            try
            {
                Łączka łącznik = new Łączka();
                łącznik.połącz();

                string polecenie;

                polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Piosenki);

                MySql.Data.MySqlClient.MySqlDataReader kursor;

                kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                while (kursor.Read())
                {
                    string[] tabDanych = new string[kursor.FieldCount];
                    for (int i = 0; i < kursor.FieldCount; i++)
                    {
                        try { tabDanych[i] = kursor.GetString(i); }
                        catch { tabDanych[i] = ""; }
                    }

                    listaPiosenek.Add(new Piosenka(tabDanych));

                    //listaPiosenek.Add(new Piosenka(kursor.GetString(0), kursor.GetString(1), kursor.GetString(2), kursor.GetString(3), kursor.GetString(4), true));
                }

                łącznik.rozłącz();
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd wczytywania danych:\n" + e.Message);
            }

            return listaPiosenek;
        }



        public void usuńZbazy()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Piosenki_w_Albumach, new string[] { Opisy.piosenki_w_albumach_id_piosenki }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);
                
                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Piosenki, new string[] { Opisy.piosenki_id_piosenki }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd usuwania danych:\n" + e.Message);
            }
        }


        public void aktualizujWbazie()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie = "";
                string warunek;

                warunek = Opisy.piosenki_id_piosenki + "=" + Id;

                polecenie = Obsługa_BD.Polecenia_SQL.aktualizujNPól(Obsługa_BD.Opisy.tab_Piosenki, new string[] { Opisy.piosenki_tytuł, Opisy.piosenki_długość }, new string[] { tytuł, długość.Długość }, warunek);
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                polecenie = Polecenia_SQL.aktualizujNPól_Specjalne(Opisy.tab_Piosenki, new string[] { Opisy.piosenki_id_zespołu, Opisy.piosenki_id_artysty }, new string[] { IdZespołu, IdArtysty }, warunek);
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd zapisu do bazy:\n" + e.Message);
            }
        }

    }

    class Długość_Piosenki
    {
        public int godziny, minuty, sekundy;

        public Długość_Piosenki(int godziny, int minuty, int sekundy)
        {
            this.godziny = godziny;
            this.minuty = minuty;
            this.sekundy = sekundy;
        }
        public Długość_Piosenki(int minuty, int sekundy)
        {
            this.godziny = 0;
            this.minuty = minuty;
            this.sekundy = sekundy;
        }
        public Długość_Piosenki(int sekundy)
        {
            this.godziny = 0;
            this.minuty = 0;
            this.sekundy = sekundy;
        }
        public Długość_Piosenki(string długośćZbazyDanych)
        {
            if (!Int32.TryParse(długośćZbazyDanych[0].ToString() + długośćZbazyDanych[1].ToString(), out this.godziny)) this.godziny = 0;
            if (!Int32.TryParse(długośćZbazyDanych[3].ToString() + długośćZbazyDanych[4].ToString(), out this.minuty)) this.minuty = 0;
            if (!Int32.TryParse(długośćZbazyDanych[6].ToString() + długośćZbazyDanych[7].ToString(), out this.sekundy)) this.sekundy = 0;
            
        }


        public string Długość
        {
            get
            {
                string długość = "";

                długość += dopiszZeroJeśliTrzeba(godziny) + godziny.ToString();
                długość += ":";

                długość += dopiszZeroJeśliTrzeba(minuty) + minuty.ToString();
                długość += ":";

                długość += dopiszZeroJeśliTrzeba(sekundy) + sekundy.ToString();

                return długość;
            }
        }

        private string dopiszZeroJeśliTrzeba(int liczba)
        {
            string tekstDoZwrotu;

            if (liczba < 10) tekstDoZwrotu = "0";
            else tekstDoZwrotu = "";

            return tekstDoZwrotu;
        }
    }
}
