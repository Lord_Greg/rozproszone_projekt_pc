﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rozproszone_Projekt_PC.Obsługa_BD;
using System.Windows.Forms;

namespace Rozproszone_Projekt_PC.Modele_Danych
{
    class Wydawca
    {
        public int id;
        public string nazwa;

        public string Id
        {
            get { return id.ToString(); }
        }

        public string Nazwa
        {
            get { return nazwa; }
        }


        public Wydawca(int id, string nazwa)
        {
            this.id = id;
            this.nazwa = nazwa;
        }

        public Wydawca(string nazwa)
        {
            this.nazwa = nazwa;
        }

        public void dodajNowyDoBazy()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;
                polecenie = Polecenia_SQL.wstawWartości(Obsługa_BD.Opisy.tab_Wydawcy, new string[] { "", nazwa });

                łącznik.wykonajPolecenieBezzwrotne(polecenie);
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd zapisu do bazy:\n" + e.Message);
            }
        }

        public static List<Wydawca> wczytajWszystkich()
        {
            List<Wydawca> listaWydawców = new List<Wydawca>();

            try
            {                
                Łączka łącznik = new Łączka();
                łącznik.połącz();

                string polecenie;

                polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Wydawcy);

                MySql.Data.MySqlClient.MySqlDataReader kursor;

                kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                while(kursor.Read())
                {
                    listaWydawców.Add(new Wydawca(kursor.GetInt32(0), kursor.GetString(1)));
                }


                łącznik.rozłącz();
            }
            catch(Exception e)
            {
                MessageBox.Show("Błąd wczytywania danych:\n" + e.Message);
            }

            return listaWydawców;
        }


        public void usuńZbazy()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;
                string warunek;

                warunek = Opisy.albumy_id_wydawcy + "=" + Id;
                polecenie = Polecenia_SQL.aktualizujNPól_Specjalne(Opisy.tab_Albumy, new string[] { Opisy.albumy_id_wydawcy }, new string[] { "null" }, warunek);
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Wydawcy, new string[] { Opisy.wydawcy_id_wydawcy }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd usuwania danych:\n" + e.Message);
            }
        }


        public void aktualizujWbazie()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;
                string warunek;

                warunek = Opisy.wydawcy_id_wydawcy + "=" + Id;
                polecenie = Obsługa_BD.Polecenia_SQL.aktualizujNPól(Obsługa_BD.Opisy.tab_Wydawcy, new string[] { Opisy.wydawcy_nazwa }, new string[] { nazwa }, warunek);
                łącznik.wykonajPolecenieBezzwrotne(polecenie);
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd zapisu do bazy:\n" + e.Message);
            }
        }


    }
}
