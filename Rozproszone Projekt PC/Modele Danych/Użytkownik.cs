﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rozproszone_Projekt_PC.Obsługa_BD;

namespace Rozproszone_Projekt_PC.Modele_Danych
{
    class Użytkownik
    {

        public string nazwa, hasło;

        public string Nazwa
        {
            get
            {
                return nazwa;
            }
        }

        public Użytkownik(string nazwa, string hasło)
        {
            this.nazwa = nazwa;
            this.hasło = hasło;
        }

        public Użytkownik(Użytkownik użytkownik)
        {
            this.nazwa = użytkownik.nazwa;
            this.hasło = użytkownik.hasło;
        }

        public void dodajNowyDoBazy()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;
                polecenie = Obsługa_BD.Polecenia_SQL.wstawWartości(Obsługa_BD.Opisy.tab_Użytkownicy, new string[] { nazwa, hasło });

                łącznik.wykonajPolecenieBezzwrotne(polecenie);
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd zapisu do bazy:\n" + e.Message);
            }
        }

        public static List<Użytkownik> wczytajWszystkich()
        {
            bool czyWszystkoSięUdało = false;
            List<Użytkownik> użytkownicy = new List<Użytkownik>();
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;
                polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Użytkownicy);

                MySql.Data.MySqlClient.MySqlDataReader kursor;

                łącznik.połącz();
                kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                while(kursor.Read())
                {
                    użytkownicy.Add(new Użytkownik(kursor.GetString(0), kursor.GetString(1)));
                }
                kursor.Close();
                łącznik.rozłącz();

                czyWszystkoSięUdało = true;
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd wczytywania z bazy:\n" + e.Message);
            }

            if(!czyWszystkoSięUdało)
            {
                użytkownicy = null;
            }

            return użytkownicy;
        }

        public bool zaloguj()
        {
            bool czyPoprawneDane = false;
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie, warunek;
                warunek = Opisy.użytkownicy_nazwa + "=\"" + nazwa + 
                          "\" AND " + Opisy.użytkownicy_hasło + "=\"" + hasło + "\"";

                polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Użytkownicy, warunek);

                MySql.Data.MySqlClient.MySqlDataReader kursor;

                łącznik.połącz();
                kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                if(kursor.Read())
                {
                    czyPoprawneDane = true;
                }
                else
                {
                    czyPoprawneDane = false;
                }
                kursor.Close();
                łącznik.rozłącz();
            }
            catch (Exception e)
            {
                czyPoprawneDane = false;
                MessageBox.Show("Błąd wczytywania z bazy:\n" + e.Message);
            }
            return czyPoprawneDane;
        }

        public void usuńZbazy()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;
                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Użytkownicy, new string[] { Opisy.użytkownicy_nazwa }, new string[] { nazwa });

                łącznik.wykonajPolecenieBezzwrotne(polecenie);
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd usuwania z bazy:\n" + e.Message);
            }
        }



    }
}