﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rozproszone_Projekt_PC.Obsługa_BD;
using System.Windows.Forms;
namespace Rozproszone_Projekt_PC.Modele_Danych
{
    class Lokalizacja
    {
        public string nazwaDziału, symbolPułki;
        public int id;
        public List<int> listaIdAlbumówNaPułce;

        public string Id
        {
            get { return id.ToString(); }
        }

        public string Pułka
        {
            get
            {
                string tekstDoWyświetlenia = "";
                if (symbolPułki != "" || nazwaDziału != "")
                {
                    tekstDoWyświetlenia += symbolPułki + " (" + nazwaDziału + ")";
                }
                return tekstDoWyświetlenia;
            }
        }

        public Lokalizacja(int id, string nazwaDziału, string symbolPułki, List<int> listaIdAlbumów)
        {
            this.id = id;
            this.nazwaDziału = nazwaDziału;
            this.symbolPułki = symbolPułki;
            this.listaIdAlbumówNaPułce = new List<int>(listaIdAlbumów);
        }

        public Lokalizacja(string id, string nazwaDziału, string symbolPułki)
        {
            this.id = Convert.ToInt32(id);
            this.nazwaDziału = nazwaDziału;
            this.symbolPułki = symbolPułki;
            listaIdAlbumówNaPułce = new List<int>();
        }

        public Lokalizacja(int id, string nazwaDziału, string symbolPułki)
        {
            this.id = id;
            this.nazwaDziału = nazwaDziału;
            this.symbolPułki = symbolPułki;
            listaIdAlbumówNaPułce = new List<int>();
        }

        public Lokalizacja(string nazwaDziału, string symbolPułki)
        {
            this.nazwaDziału = nazwaDziału;
            this.symbolPułki = symbolPułki;
            listaIdAlbumówNaPułce = new List<int>();
        }
        

        public void dodajNowyDoBazy()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie = "";

                polecenie += Polecenia_SQL.wstawWartości(Opisy.tab_Lokalizacje, new string[] { "", nazwaDziału, symbolPułki });
                
                łącznik.wykonajPolecenieBezzwrotne(polecenie);


                polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Lokalizacje, Opisy.lokalizacje_nazwa_działu + '=' + '"' + nazwaDziału + '"' + " AND " + Opisy.lokalizacje_symbol_półki + '=' + '"' + symbolPułki + '"' + " ORDER BY " + Opisy.lokalizacje_id_lokalizacji + " DESC");
                łącznik.połącz();

                MySql.Data.MySqlClient.MySqlDataReader kursor = łącznik.wykonajPolecenieZwrotne(polecenie);
                kursor.Read();
                id = kursor.GetInt32(0);
                kursor.Close();
                łącznik.rozłącz();

                //Lokalizacje Albumów
                foreach (int idAlbumu in listaIdAlbumówNaPułce)
                {
                    polecenie = Polecenia_SQL.wstawWartości(Opisy.tab_Lokalizacje_Albumów, new string[] { id.ToString(), idAlbumu.ToString() });
                    łącznik.wykonajPolecenieBezzwrotne(polecenie);
                }
                

            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd zapisu do bazy:\n" + e.Message);
            }
            
        }


        public static List<Lokalizacja> wczytajWszystkoZbazy()
        {
            List<Lokalizacja> lokalizacje = new List<Lokalizacja>();

            Łączka łącznik = new Łączka();
            string polecenie;
            string warunek;
            polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Lokalizacje);

            łącznik.połącz();
            MySql.Data.MySqlClient.MySqlDataReader kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

            while (kursor.Read())
            {
                lokalizacje.Add(new Lokalizacja(kursor.GetString(0), kursor.GetString(1), kursor.GetString(2)));
            }
            kursor.Close();

            foreach (Lokalizacja lokalizacja in lokalizacje)
            {
                warunek = Opisy.lokalizacje_albumów_id_lokalizacji + "=" + lokalizacja.Id;
                polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Lokalizacje_Albumów, warunek);
                kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                while (kursor.Read())
                {
                    lokalizacja.listaIdAlbumówNaPułce.Add(kursor.GetInt32(1));
                }
                kursor.Close();
            }
            
            łącznik.rozłącz();

            return lokalizacje;
        }


        public void usuńZbazy()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Lokalizacje_Albumów, new string[] { Opisy.lokalizacje_albumów_id_lokalizacji }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Lokalizacje, new string[] { Opisy.lokalizacje_id_lokalizacji }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd usuwania danych:\n" + e.Message);
            }
        }


        public void aktualizujWbazie(Lokalizacja poprzedniaLokalizacja)
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie = "";
                string warunek;

                warunek = Opisy.lokalizacje_nazwa_działu + "=\"" + poprzedniaLokalizacja.nazwaDziału + "\"";
                polecenie = Polecenia_SQL.aktualizujNPól(Opisy.tab_Lokalizacje, new string[] { Opisy.lokalizacje_nazwa_działu }, new string[] { nazwaDziału }, warunek);
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                warunek = Opisy.lokalizacje_id_lokalizacji + "=" + Id;
                polecenie = Polecenia_SQL.aktualizujNPól(Opisy.tab_Lokalizacje, new string[] { Opisy.lokalizacje_symbol_półki }, new string[] { symbolPułki }, warunek);
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                //Lokalizacje Albumów
                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Lokalizacje_Albumów, new string[] { Opisy.lokalizacje_albumów_id_lokalizacji }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                foreach (int idAlbumu in listaIdAlbumówNaPułce)
                {
                    try
                    {
                        polecenie = Polecenia_SQL.wstawWartości(Opisy.tab_Lokalizacje_Albumów, new string[] { id.ToString(), idAlbumu.ToString() });
                        łącznik.wykonajPolecenieBezzwrotne(polecenie);
                    }
                    catch (Exception e) { }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd zapisu do bazy:\n" + e.Message);
            }
        }

    }
}