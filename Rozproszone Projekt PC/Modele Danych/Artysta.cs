﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rozproszone_Projekt_PC.Obsługa_BD;
using System.Windows.Forms;

namespace Rozproszone_Projekt_PC.Modele_Danych
{
    class Artysta
    {
        public string imie, nazwisko, pseudonim, narodowość;
        public int id;

        public string Id
        {
            get { return id.ToString(); }
        }

        public string Nazwa
        {
            get
            {
                string tekstDoZwrotu = "";
                if(imie != null && imie != "")
                {
                    tekstDoZwrotu += imie;
                }

                if (pseudonim != null && pseudonim != "")
                {
                    if(tekstDoZwrotu != "")
                    {
                        tekstDoZwrotu += " ";
                    }

                    tekstDoZwrotu += "\"" + pseudonim + "\"";
                }

                if (nazwisko != null && nazwisko != "")
                {
                    if (tekstDoZwrotu != "")
                    {
                        tekstDoZwrotu += " ";
                    }

                    tekstDoZwrotu += nazwisko;
                }
                return tekstDoZwrotu;
            }
        }


        public Artysta(string imie, string nazwisko, string pseudonim, string narodowość)
        {
            this.imie = imie;
            this.nazwisko = nazwisko;
            this.pseudonim = pseudonim;
            this.narodowość = narodowość;
        }

        public Artysta(int id, string imie, string nazwisko, string pseudonim, string narodowość)
        {
            this.id = id;
            this.imie = imie;
            this.nazwisko = nazwisko;
            this.pseudonim = pseudonim;
            this.narodowość = narodowość;
        }


        public void dodajNowyDoBazy()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;
                polecenie = Obsługa_BD.Polecenia_SQL.wstawWartości(Obsługa_BD.Opisy.tab_Artyści, new string[] { "", imie, nazwisko, pseudonim, narodowość });

                łącznik.wykonajPolecenieBezzwrotne(polecenie);
            }
            catch(Exception e)
            {
                MessageBox.Show("Błąd zapisu do bazy:\n" + e.Message);
            }
        }


        public static List<Artysta> wczytajWszystkich()
        {
            List<Artysta> listaArtystów = new List<Artysta>();

            try
            {
                Łączka łącznik = new Łączka();
                łącznik.połącz();

                string polecenie;

                polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Artyści);

                MySql.Data.MySqlClient.MySqlDataReader kursor;

                kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                while (kursor.Read())
                {
                    listaArtystów.Add(new Artysta(kursor.GetInt32(0), kursor.GetString(1), kursor.GetString(2), kursor.GetString(3), kursor.GetString(4)));
                }


                łącznik.rozłącz();
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd wczytywania danych:\n" + e.Message);
            }

            return listaArtystów;
        }


        public void usuńZbazy()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Artyści_w_Zespołach, new string[] { Opisy.artyści_w_zespołach_id_artysty }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Artyści_w_Albumach, new string[] { Opisy.artyści_w_albumach_id_artysty }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                string warunek = Opisy.piosenki_id_artysty + "=" + Id;
                polecenie = Polecenia_SQL.aktualizujNPól_Specjalne(Opisy.tab_Piosenki, new string[] { Opisy.piosenki_id_artysty }, new string[] { "null" }, warunek);
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Artyści, new string[] { Opisy.artyści_id_artysty }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd usuwania danych:\n" + e.Message);
            }
        }


        public void aktualizujWbazie()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;
                string warunek;

                warunek = Opisy.artyści_id_artysty + "=" + Id;
                polecenie = Obsługa_BD.Polecenia_SQL.aktualizujNPól(Obsługa_BD.Opisy.tab_Artyści, new string[] { Opisy.artyści_imię, Opisy.artyści_nazwisko, Opisy.artyści_pseudonim, Opisy.artyści_narodowość }, new string[] { imie, nazwisko, pseudonim, narodowość }, warunek);
                łącznik.wykonajPolecenieBezzwrotne(polecenie);
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd zapisu do bazy:\n" + e.Message);
            }
        }

    }
}
