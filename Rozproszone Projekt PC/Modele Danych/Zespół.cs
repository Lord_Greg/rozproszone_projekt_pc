﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rozproszone_Projekt_PC.Obsługa_BD;
using System.Windows.Forms;

namespace Rozproszone_Projekt_PC.Modele_Danych
{
    class Zespół
    {
        public string nazwa, pochodzenie;
        public int id, rokZałożenia;
        public List<Artysta> listaCzłonków;

        public string Nazwa
        {
            get { return nazwa; }
        }

        public string Id
        {
            get { return id.ToString(); }
        }


        public Zespół(string nazwa, string rokZałożenia, string pochodzenie)
        {
            this.nazwa = nazwa;
            Int32.TryParse(rokZałożenia, out this.rokZałożenia);
            this.pochodzenie = pochodzenie;
            this.listaCzłonków = new List<Artysta>();
        }

        public Zespół(string nazwa, string rokZałożenia, string pochodzenie, List<Artysta> listaCzłonków)
        {
            this.nazwa = nazwa;
            Int32.TryParse(rokZałożenia, out this.rokZałożenia);
            this.pochodzenie = pochodzenie;
            this.listaCzłonków = new List<Artysta>(listaCzłonków);
        }

        public Zespół(int id, string nazwa, string rokZałożenia, string pochodzenie, List<Artysta> listaCzłonków)
        {
            this.id = id;
            this.nazwa = nazwa;
            Int32.TryParse(rokZałożenia, out this.rokZałożenia);
            this.pochodzenie = pochodzenie;
            this.listaCzłonków = new List<Artysta>(listaCzłonków);
        }

        public Zespół(int id, string nazwa, int rokZałożenia, string pochodzenie)
        {
            this.id = id;
            this.nazwa = nazwa;
            this.rokZałożenia = rokZałożenia;
            this.pochodzenie = pochodzenie;
            this.listaCzłonków = new List<Artysta>();
        }


        public void dodajNowyDoBazy()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;
                polecenie = Polecenia_SQL.wstawWartości(Opisy.tab_Zespoły, new string[] { "", nazwa, rokZałożenia.ToString(), pochodzenie });

                łącznik.wykonajPolecenieBezzwrotne(polecenie);


                polecenie = Polecenia_SQL.wczytajCałąTabele(
                    Opisy.tab_Zespoły, Opisy.zespoły_nazwa + 
                    "=\"" + nazwa + "\" ORDER BY " + Opisy.zespoły_id_zespołu + " DESC" );
                łącznik.połącz();
                MySql.Data.MySqlClient.MySqlDataReader kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                if(kursor.Read())
                    id = kursor.GetInt32(0);

                kursor.Close();
                łącznik.rozłącz();

                //Dodawanie członków
                foreach(Artysta członek in listaCzłonków)
                {
                    if (członek.id > 0)
                    {
                        try
                        {
                            polecenie = Polecenia_SQL.wstawWartości(
                                        Opisy.tab_Artyści_w_Zespołach, 
                                        new string[] { id.ToString(), członek.id.ToString() });
                            łącznik.wykonajPolecenieBezzwrotne(polecenie);
                        }
                        catch (Exception e) { }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd zapisu do bazy:\n" + e.Message);
            }
        }


        public static List<Zespół> wczytajWszystkie()
        {
            List<Zespół> listaZespołów = new List<Zespół>();
            
            try
            {
                Łączka łącznik = new Łączka();
                łącznik.połącz();

                string polecenie;

                polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Zespoły);

                MySql.Data.MySqlClient.MySqlDataReader kursor;

                kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                while (kursor.Read())
                {
                    listaZespołów.Add(new Zespół(kursor.GetInt32(0), kursor.GetString(1), kursor.GetInt32(2), kursor.GetString(3)));
                }
                kursor.Close();


                foreach (Zespół zespół in listaZespołów)
                {
                    string warunek =
                        Opisy.tab_Artyści_w_Zespołach + "." + Opisy.artyści_w_zespołach_id_zespołu + "=" + zespół.Id
                        + " AND " +
                        Opisy.tab_Artyści + "." + Opisy.artyści_id_artysty + 
                        "=" + 
                        Opisy.tab_Artyści_w_Zespołach + "." + Opisy.artyści_w_zespołach_id_artysty;

                    polecenie = Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Artyści + ", " + Opisy.tab_Artyści_w_Zespołach, warunek);
                    kursor = łącznik.wykonajPolecenieZwrotne(polecenie);

                    while (kursor.Read())
                    {
                        zespół.listaCzłonków.Add(new Artysta(kursor.GetInt32(0), kursor.GetString(1),
                                        kursor.GetString(2), kursor.GetString(3), kursor.GetString(4)));
                    }

                    kursor.Close();
                }

                łącznik.rozłącz();
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd wczytywania danych:\n" + e.Message);
            }

            return listaZespołów;
        }


        public void usuńZbazy()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Artyści_w_Zespołach,
                    new string[] { Opisy.artyści_w_zespołach_id_zespołu }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Zespoły_w_Albumach,
                    new string[] { Opisy.zespoły_w_albumach_id_zespołu }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                string warunek = Opisy.piosenki_id_zespołu + "=" + Id;
                polecenie = Polecenia_SQL.aktualizujNPól_Specjalne(Opisy.tab_Piosenki,
                    new string[] { Opisy.piosenki_id_zespołu }, new string[] { "null" }, warunek);
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Zespoły,
                    new string[] { Opisy.zespoły_id_zespołu }, new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);
            }
            catch(Exception e)
            {
                MessageBox.Show("Błąd usuwania danych:\n" + e.Message);
            }
        }


        public void aktualizujWbazie()
        {
            try
            {
                Łączka łącznik = new Łączka();
                string polecenie;
                string warunek;

                warunek = Opisy.zespoły_id_zespołu + "=" + Id;
                polecenie = Polecenia_SQL.aktualizujNPól(Opisy.tab_Zespoły,
                    new string[] { Opisy.zespoły_nazwa, Opisy.zespoły_rok_założenia, Opisy.zespoły_pochodzenie },
                    new string[] { nazwa, rokZałożenia.ToString(), pochodzenie }, warunek);
                łącznik.wykonajPolecenieBezzwrotne(polecenie);

                //Aktualizuj członków
                polecenie = Polecenia_SQL.usuńRekordy(Opisy.tab_Artyści_w_Zespołach,
                    new string[] { Opisy.artyści_w_zespołach_id_zespołu },
                    new string[] { Id });
                łącznik.wykonajPolecenieBezzwrotne(polecenie);
                foreach (Artysta członek in listaCzłonków)
                {
                    if (członek.id > 0)
                    {
                        try
                        {
                            polecenie = Polecenia_SQL.wstawWartości(Opisy.tab_Artyści_w_Zespołach,
                                new string[] { id.ToString(), członek.id.ToString() });
                            łącznik.wykonajPolecenieBezzwrotne(polecenie);
                        }
                        catch (Exception e) { }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd zapisu do bazy:\n" + e.Message);
            }
        }
    }
}
