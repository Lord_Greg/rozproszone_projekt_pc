﻿namespace Rozproszone_Projekt_PC.Formatki
{
    partial class Zarządzaj_Artystami
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxImię = new System.Windows.Forms.TextBox();
            this.tbxNazwisko = new System.Windows.Forms.TextBox();
            this.tbxPseudonim = new System.Windows.Forms.TextBox();
            this.tbxNarodowość = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnUsuń = new System.Windows.Forms.Button();
            this.cbxWybierzArtyste = new System.Windows.Forms.ComboBox();
            this.btnPoprzedni = new System.Windows.Forms.Button();
            this.btnNastępny = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbxImię
            // 
            this.tbxImię.Location = new System.Drawing.Point(12, 44);
            this.tbxImię.Name = "tbxImię";
            this.tbxImię.Size = new System.Drawing.Size(155, 20);
            this.tbxImię.TabIndex = 0;
            // 
            // tbxNazwisko
            // 
            this.tbxNazwisko.Location = new System.Drawing.Point(12, 92);
            this.tbxNazwisko.Name = "tbxNazwisko";
            this.tbxNazwisko.Size = new System.Drawing.Size(155, 20);
            this.tbxNazwisko.TabIndex = 1;
            // 
            // tbxPseudonim
            // 
            this.tbxPseudonim.Location = new System.Drawing.Point(12, 137);
            this.tbxPseudonim.Name = "tbxPseudonim";
            this.tbxPseudonim.Size = new System.Drawing.Size(155, 20);
            this.tbxPseudonim.TabIndex = 2;
            // 
            // tbxNarodowość
            // 
            this.tbxNarodowość.Location = new System.Drawing.Point(12, 184);
            this.tbxNarodowość.Name = "tbxNarodowość";
            this.tbxNarodowość.Size = new System.Drawing.Size(155, 20);
            this.tbxNarodowość.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Imię";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Nazwisko";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Pseudonim Artystyczny";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 168);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Narodowość";
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(118, 224);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(75, 23);
            this.btnDodaj.TabIndex = 8;
            this.btnDodaj.Text = "Zapisz";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnUsuń
            // 
            this.btnUsuń.Location = new System.Drawing.Point(127, 253);
            this.btnUsuń.Name = "btnUsuń";
            this.btnUsuń.Size = new System.Drawing.Size(56, 23);
            this.btnUsuń.TabIndex = 35;
            this.btnUsuń.Text = "Usuń";
            this.btnUsuń.UseVisualStyleBackColor = true;
            this.btnUsuń.Click += new System.EventHandler(this.btnUsuń_Click);
            // 
            // cbxWybierzArtyste
            // 
            this.cbxWybierzArtyste.FormattingEnabled = true;
            this.cbxWybierzArtyste.Location = new System.Drawing.Point(82, 12);
            this.cbxWybierzArtyste.Name = "cbxWybierzArtyste";
            this.cbxWybierzArtyste.Size = new System.Drawing.Size(151, 21);
            this.cbxWybierzArtyste.TabIndex = 34;
            this.cbxWybierzArtyste.SelectedIndexChanged += new System.EventHandler(this.cbxWybierzArtyste_SelectedIndexChanged);
            // 
            // btnPoprzedni
            // 
            this.btnPoprzedni.Location = new System.Drawing.Point(12, 239);
            this.btnPoprzedni.Name = "btnPoprzedni";
            this.btnPoprzedni.Size = new System.Drawing.Size(75, 23);
            this.btnPoprzedni.TabIndex = 33;
            this.btnPoprzedni.Text = "<Poprzedni<";
            this.btnPoprzedni.UseVisualStyleBackColor = true;
            this.btnPoprzedni.Click += new System.EventHandler(this.btnPoprzedni_Click);
            // 
            // btnNastępny
            // 
            this.btnNastępny.Location = new System.Drawing.Point(224, 239);
            this.btnNastępny.Name = "btnNastępny";
            this.btnNastępny.Size = new System.Drawing.Size(75, 23);
            this.btnNastępny.TabIndex = 32;
            this.btnNastępny.Text = ">Następny>";
            this.btnNastępny.UseVisualStyleBackColor = true;
            this.btnNastępny.Click += new System.EventHandler(this.btnNastępny_Click);
            // 
            // Zarządzaj_Artystami
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 279);
            this.Controls.Add(this.btnUsuń);
            this.Controls.Add(this.cbxWybierzArtyste);
            this.Controls.Add(this.btnPoprzedni);
            this.Controls.Add(this.btnNastępny);
            this.Controls.Add(this.btnDodaj);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxNarodowość);
            this.Controls.Add(this.tbxPseudonim);
            this.Controls.Add(this.tbxNazwisko);
            this.Controls.Add(this.tbxImię);
            this.Name = "Zarządzaj_Artystami";
            this.Text = "Zarządzanie Artystami";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxImię;
        private System.Windows.Forms.TextBox tbxNazwisko;
        private System.Windows.Forms.TextBox tbxPseudonim;
        private System.Windows.Forms.TextBox tbxNarodowość;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnUsuń;
        private System.Windows.Forms.ComboBox cbxWybierzArtyste;
        private System.Windows.Forms.Button btnPoprzedni;
        private System.Windows.Forms.Button btnNastępny;
    }
}