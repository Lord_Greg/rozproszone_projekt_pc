﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rozproszone_Projekt_PC.Modele_Danych;

namespace Rozproszone_Projekt_PC.Formatki
{
    public partial class Zarządzaj_Gatunkami : Form
    {
        private List<Gatunek> listaGatunków;


        public Zarządzaj_Gatunkami()
        {
            InitializeComponent();

            przeładujGatunki();
            wyczyśćKontrolki();
            btnUsuń.Visible = false;
        }


        #region Zdarzenia Kontrolek

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            DialogResult dlgCzyZapisać = MessageBox.Show("Czy na pewno chcesz zapisać zmianę?", "Zapisz Zmianę", MessageBoxButtons.YesNo);
            if (dlgCzyZapisać == DialogResult.Yes)
            {
                Gatunek gatunek = new Gatunek(tbxNazwa.Text);

                if (Poprawności_Danych.czyPodanoPoprawneDane(gatunek))
                {
                    if (cbxWybierzGatunek.SelectedIndex != 0)
                    {
                        //Zmodyfikuj Istniejący
                        try
                        {
                            gatunek = (Gatunek)cbxWybierzGatunek.SelectedItem;
                            gatunek.aktualizujWbazie(tbxNazwa.Text);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Wystąpił błąd:\n" + ex.Message);
                        }
                    }
                    else
                    {
                        //Dodaj Nowy
                        if (Poprawności_Danych.czyPodanoPoprawneDane(gatunek))
                        {
                            try
                            {
                                gatunek.dodajNowyDoBazy();
                                wyczyśćKontrolki();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Wystąpił błąd:\n" + ex.Message);
                            }
                        }
                    }

                    przeładujGatunki();
                }
            }
            else if (dlgCzyZapisać == DialogResult.No)
            {
                //nie zapisuj
            }
        }
        
        private void btnNastępny_Click(object sender, EventArgs e)
        {
            if (cbxWybierzGatunek.SelectedIndex + 1 < cbxWybierzGatunek.Items.Count)
                cbxWybierzGatunek.SelectedIndex++;
            else
                cbxWybierzGatunek.SelectedIndex = 0;
        }

        private void btnPoprzedni_Click(object sender, EventArgs e)
        {
            if (cbxWybierzGatunek.SelectedIndex - 1 >= 0)
                cbxWybierzGatunek.SelectedIndex--;
            else
                if (cbxWybierzGatunek.Items.Count > 0) cbxWybierzGatunek.SelectedIndex = cbxWybierzGatunek.Items.Count - 1;
            else cbxWybierzGatunek.SelectedIndex = 0;
        }

        private void btnUsuń_Click(object sender, EventArgs e)
        {
            DialogResult dlgCzyUsunąć = MessageBox.Show("Czy na pewno chcesz usunąć ten gatunek?", "Usuń Gatunek", MessageBoxButtons.YesNo);
            if (dlgCzyUsunąć == DialogResult.Yes)
            {
                ((Gatunek)cbxWybierzGatunek.SelectedItem).usuńZbazy();
                przeładujGatunki();
            }
            else if (dlgCzyUsunąć == DialogResult.No)
            {
                //nie usuwaj
            }
        }


        private void cbxWybierzGatunek_SelectedIndexChanged(object sender, EventArgs e)
        {
            wczytajDaneGatunku((Gatunek)cbxWybierzGatunek.SelectedItem);

            if (cbxWybierzGatunek.SelectedIndex == 0)
            {
                wyczyśćKontrolki();
                btnUsuń.Visible = false;
            }
            else
            {
                btnUsuń.Visible = true;
            }
        }

        #endregion


        private void wyczyśćKontrolki()
        {
            tbxNazwa.Text = "";
        }


        private void przeładujGatunki()
        {
            listaGatunków = new List<Gatunek>();
            listaGatunków.AddRange(Gatunek.wczytajWszystkoZbazy());
            listaGatunków.Sort((x, y) => x.Nazwa.CompareTo(y.Nazwa));
            listaGatunków.Insert(0, new Gatunek("<Nowy Gatunek>"));

            cbxWybierzGatunek.DataSource = listaGatunków;
            cbxWybierzGatunek.DisplayMember = "Nazwa";
            cbxWybierzGatunek.ValueMember = "Nazwa";
        }


        private void wczytajDaneGatunku(Gatunek gatunek)
        {
            if (gatunek == null)
            {
                if (listaGatunków.Count > 0) gatunek = listaGatunków.ElementAt(0);
                else return;
            }

            tbxNazwa.Text = gatunek.nazwa;
        }

    }
}