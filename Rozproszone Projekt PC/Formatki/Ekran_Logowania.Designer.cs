﻿namespace Rozproszone_Projekt_PC.Formatki
{
    partial class Ekran_Logowania
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxNazwaUżytkownika = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.mtbxHasło = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnZaloguj = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbxNazwaUżytkownika
            // 
            this.tbxNazwaUżytkownika.Location = new System.Drawing.Point(39, 33);
            this.tbxNazwaUżytkownika.Name = "tbxNazwaUżytkownika";
            this.tbxNazwaUżytkownika.Size = new System.Drawing.Size(138, 20);
            this.tbxNazwaUżytkownika.TabIndex = 36;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 39;
            this.label1.Text = "Nazwa Użytkownika";
            // 
            // mtbxHasło
            // 
            this.mtbxHasło.Location = new System.Drawing.Point(39, 85);
            this.mtbxHasło.Name = "mtbxHasło";
            this.mtbxHasło.PasswordChar = '*';
            this.mtbxHasło.Size = new System.Drawing.Size(138, 20);
            this.mtbxHasło.TabIndex = 42;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 13);
            this.label6.TabIndex = 41;
            this.label6.Text = "Hasło";
            // 
            // btnZaloguj
            // 
            this.btnZaloguj.Location = new System.Drawing.Point(71, 124);
            this.btnZaloguj.Name = "btnZaloguj";
            this.btnZaloguj.Size = new System.Drawing.Size(75, 23);
            this.btnZaloguj.TabIndex = 44;
            this.btnZaloguj.Text = "Zaloguj";
            this.btnZaloguj.UseVisualStyleBackColor = true;
            this.btnZaloguj.Click += new System.EventHandler(this.btnZaloguj_Click);
            // 
            // Ekran_Logowania
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(221, 165);
            this.Controls.Add(this.btnZaloguj);
            this.Controls.Add(this.tbxNazwaUżytkownika);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.mtbxHasło);
            this.Controls.Add(this.label6);
            this.Name = "Ekran_Logowania";
            this.Text = "Logowanie";
            this.Load += new System.EventHandler(this.Ekran_Logowania_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox tbxNazwaUżytkownika;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox mtbxHasło;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnZaloguj;
    }
}