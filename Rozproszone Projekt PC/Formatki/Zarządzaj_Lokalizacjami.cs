﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rozproszone_Projekt_PC.Modele_Danych;

namespace Rozproszone_Projekt_PC.Formatki
{
    public partial class Zarządzaj_Lokalizacjami : Form
    {
        private List<Lokalizacja> listaLokalizacji;
        private List<string> listaDziałów;
        private List<string> listaPułek;
        private List<Album> listaAlbumów;

        public Zarządzaj_Lokalizacjami()
        {
            InitializeComponent();

            załadujAlbumy();
            wczytajLokalizacje();
            
            wyczyśćKontrolki();
            btnUsuń.Visible = false;
        }


        #region Zdarzenia Kontrolek

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            DialogResult dlgCzyZapisać = MessageBox.Show("Czy na pewno chcesz zapisać zmianę?", "Zapisz Zmianę", MessageBoxButtons.YesNo);
            if (dlgCzyZapisać == DialogResult.Yes)
            {
                List<int> idAlbumów = new List<int>();
                for(int i=0; i<dgvAlbumy.RowCount; i++)
                {
                    if(dgvAlbumy.Rows[i].Cells[0].Value != null && dgvAlbumy.Rows[i].Cells[0].Value.ToString() != "" && dgvAlbumy.Rows[i].Cells[0].Value.ToString() != "0")
                    {
                        int wartość;
                        Int32.TryParse(dgvAlbumy.Rows[i].Cells[0].Value.ToString(), out wartość);
                        idAlbumów.Add(wartość);
                    }
                }

                int idLokalizacji;
                if (cbxWybierzDział.SelectedIndex > 0 && cbxWybierzPułke.SelectedIndex > 0)
                {
                    idLokalizacji = listaLokalizacji.Find(x => x.nazwaDziału == cbxWybierzDział.SelectedValue.ToString() && x.symbolPułki == cbxWybierzPułke.SelectedValue.ToString()).id;
                }
                else
                {
                    idLokalizacji = 0;
                }

                Lokalizacja lokalizacja = new Lokalizacja(idLokalizacji, tbxNazwaDziału.Text, tbxSymbolPułki.Text, idAlbumów);

                if (Poprawności_Danych.czyPodanoPoprawneDane(lokalizacja))
                {
                    if (lokalizacja.id > 0)
                    {
                        //Zmodyfikuj Istniejący
                        try
                        {
                            lokalizacja.aktualizujWbazie(listaLokalizacji.Find(x => x.id == idLokalizacji));
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Wystąpił błąd:\n" + ex.Message);
                        }
                    }
                    else
                    {
                        //Dodaj Nowy
                        try
                        {
                            lokalizacja.dodajNowyDoBazy();
                            wyczyśćKontrolki();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Wystąpił błąd:\n" + ex.Message);
                        }
                    }

                    wczytajLokalizacje();
                }
            }
            else if (dlgCzyZapisać == DialogResult.No)
            {
                //nie zapisuj
            }   
        }

        private void btnUsuń_Click(object sender, EventArgs e)
        {
            DialogResult dlgCzyUsunąć = MessageBox.Show("Czy na pewno chcesz usunąć tą lokalizację?", "Usuń Lokalizację", MessageBoxButtons.YesNo);
            if (dlgCzyUsunąć == DialogResult.Yes)
            {
                listaLokalizacji.Find(x => x.nazwaDziału == cbxWybierzDział.SelectedValue.ToString() && x.symbolPułki == cbxWybierzPułke.SelectedValue.ToString()).usuńZbazy();
                wczytajLokalizacje();
            }
            else if (dlgCzyUsunąć == DialogResult.No)
            {
                //nie usuwaj
            }
        }

        private void btnNastępny_Click(object sender, EventArgs e)
        {
            if (cbxWybierzPułke.SelectedIndex + 1 < cbxWybierzPułke.Items.Count)
                cbxWybierzPułke.SelectedIndex++;
            else
            {
                cbxWybierzPułke.SelectedIndex = 0;
                if (cbxWybierzDział.SelectedIndex + 1 < cbxWybierzDział.Items.Count)
                    cbxWybierzDział.SelectedIndex++;
                else
                    cbxWybierzDział.SelectedIndex = 0;
            }
        }

        private void btnPoprzedni_Click(object sender, EventArgs e)
        {
            if (cbxWybierzPułke.SelectedIndex - 1 >= 0)
                cbxWybierzPułke.SelectedIndex--;
            else
            {
                if (cbxWybierzDział.SelectedIndex - 1 >= 0) cbxWybierzDział.SelectedIndex--;
                else cbxWybierzDział.SelectedIndex = cbxWybierzDział.Items.Count - 1;

                cbxWybierzPułke.SelectedIndex = cbxWybierzPułke.Items.Count - 1;
            }
        }


        private void cbxWybierzDział_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxWybierzDział.SelectedIndex != 0)
            {
                tbxNazwaDziału.Text = cbxWybierzDział.SelectedValue.ToString();
                załadujPułki((string)cbxWybierzDział.SelectedItem);
            }
            else
            {
                wyczyśćKontrolki();
                załadujPułki("");
            }
        }

        private void cbxWybierzPułke_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxWybierzDział.SelectedIndex != 0 && cbxWybierzPułke.SelectedIndex != 0)
            {
                btnUsuń.Visible = true;

                tbxSymbolPułki.Text = cbxWybierzPułke.SelectedValue.ToString();
                załadujAlbumyDoGrida(listaLokalizacji.Find(x => x.nazwaDziału == cbxWybierzDział.SelectedValue.ToString() && x.symbolPułki == cbxWybierzPułke.SelectedValue.ToString()));
            }
            else
            {
                btnUsuń.Visible = false;

                tbxSymbolPułki.Text = "";
                dgvAlbumy.Rows.Clear();
            }
        }

        #endregion


        private void wczytajLokalizacje()
        {
            listaLokalizacji = new List<Lokalizacja>();
            listaLokalizacji.AddRange(Lokalizacja.wczytajWszystkoZbazy());

            załadujDziały();
        }


        private void załadujDziały()
        {
            listaDziałów = new List<string>();

            foreach (Lokalizacja lokalizacja in listaLokalizacji)
            {
                bool czyDodaćDział = true;
                foreach (string dział in listaDziałów)
                {
                    if (dział == lokalizacja.nazwaDziału)
                    {
                        czyDodaćDział = false;
                        break;
                    }
                }

                if (czyDodaćDział)
                {
                    listaDziałów.Add(lokalizacja.nazwaDziału);
                }
            }

            listaDziałów.Sort((x, y) => (x).CompareTo(y));

            listaDziałów.Insert(0, "<Nowy Dział>");

            cbxWybierzDział.DataSource = listaDziałów;
        }

        private void załadujPułki(string nazwaDziału)
        {
            listaPułek = new List<string>();

            foreach (Lokalizacja lokalizacja in listaLokalizacji)
            {
                if (lokalizacja.nazwaDziału == nazwaDziału)
                {
                    listaPułek.Add(lokalizacja.symbolPułki);
                }
            }

            listaPułek.Sort((x, y) => (x).CompareTo(y));

            listaPułek.Insert(0, "<Nowa Pułka>");

            cbxWybierzPułke.DataSource = listaPułek;
        }

        private void załadujAlbumy()
        {
            listaAlbumów = new List<Album>();
            listaAlbumów.AddRange(Album.wczytajWszystkie());

            listaAlbumów.Sort((x, y) => x.NazwaZwykonawcą.CompareTo(y.NazwaZwykonawcą));

            listaAlbumów.Insert(0, new Album(0, ""));

            dgvcAlbum.DataSource = listaAlbumów;
            dgvcAlbum.DisplayMember = "NazwaZwykonawcą";
            dgvcAlbum.ValueMember = "Id";
        }

        private void załadujAlbumyDoGrida(Lokalizacja lokalizacja)
        {
            dgvAlbumy.Rows.Clear();

            int i = 0;
            foreach (int idAlbumu in lokalizacja.listaIdAlbumówNaPułce)
            {
                dgvAlbumy.Rows.Add();
                dgvAlbumy.Rows[i].Cells[0].Value = idAlbumu.ToString();
                i++;
            }
        }



        private void wyczyśćKontrolki()
        {
            tbxNazwaDziału.Text = "";
            tbxSymbolPułki.Text = "";
            dgvAlbumy.Rows.Clear();
        }

    }
}
