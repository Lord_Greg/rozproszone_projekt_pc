﻿namespace Rozproszone_Projekt_PC.Formatki
{
    partial class Zarządzaj_Piosenkami
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDodaj = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxNazwa = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.nudSekundy = new System.Windows.Forms.NumericUpDown();
            this.nudMinuty = new System.Windows.Forms.NumericUpDown();
            this.nudGodziny = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbxSolista = new System.Windows.Forms.ComboBox();
            this.cbxZespół = new System.Windows.Forms.ComboBox();
            this.rbtWszystko = new System.Windows.Forms.RadioButton();
            this.rbtZespół = new System.Windows.Forms.RadioButton();
            this.rbtSolista = new System.Windows.Forms.RadioButton();
            this.btnUsuń = new System.Windows.Forms.Button();
            this.cbxWybierzPiosenke = new System.Windows.Forms.ComboBox();
            this.btnPoprzedni = new System.Windows.Forms.Button();
            this.btnNastępny = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSekundy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinuty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGodziny)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(126, 179);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(75, 23);
            this.btnDodaj.TabIndex = 24;
            this.btnDodaj.Text = "Zapisz";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Tytuł";
            // 
            // tbxNazwa
            // 
            this.tbxNazwa.Location = new System.Drawing.Point(12, 51);
            this.tbxNazwa.Name = "tbxNazwa";
            this.tbxNazwa.Size = new System.Drawing.Size(147, 20);
            this.tbxNazwa.TabIndex = 22;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nudSekundy);
            this.groupBox1.Controls.Add(this.nudMinuty);
            this.groupBox1.Controls.Add(this.nudGodziny);
            this.groupBox1.Location = new System.Drawing.Point(174, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(161, 49);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Czas Trwania";
            // 
            // nudSekundy
            // 
            this.nudSekundy.Location = new System.Drawing.Point(119, 19);
            this.nudSekundy.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.nudSekundy.Name = "nudSekundy";
            this.nudSekundy.Size = new System.Drawing.Size(34, 20);
            this.nudSekundy.TabIndex = 2;
            // 
            // nudMinuty
            // 
            this.nudMinuty.Location = new System.Drawing.Point(64, 19);
            this.nudMinuty.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.nudMinuty.Name = "nudMinuty";
            this.nudMinuty.Size = new System.Drawing.Size(34, 20);
            this.nudMinuty.TabIndex = 1;
            // 
            // nudGodziny
            // 
            this.nudGodziny.Location = new System.Drawing.Point(6, 19);
            this.nudGodziny.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.nudGodziny.Name = "nudGodziny";
            this.nudGodziny.Size = new System.Drawing.Size(34, 20);
            this.nudGodziny.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbxSolista);
            this.groupBox2.Controls.Add(this.cbxZespół);
            this.groupBox2.Controls.Add(this.rbtWszystko);
            this.groupBox2.Controls.Add(this.rbtZespół);
            this.groupBox2.Controls.Add(this.rbtSolista);
            this.groupBox2.Location = new System.Drawing.Point(7, 87);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(327, 82);
            this.groupBox2.TabIndex = 64;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Wykonawca";
            // 
            // cbxSolista
            // 
            this.cbxSolista.FormattingEnabled = true;
            this.cbxSolista.Location = new System.Drawing.Point(200, 42);
            this.cbxSolista.Name = "cbxSolista";
            this.cbxSolista.Size = new System.Drawing.Size(121, 21);
            this.cbxSolista.TabIndex = 64;
            // 
            // cbxZespół
            // 
            this.cbxZespół.FormattingEnabled = true;
            this.cbxZespół.Location = new System.Drawing.Point(6, 42);
            this.cbxZespół.Name = "cbxZespół";
            this.cbxZespół.Size = new System.Drawing.Size(121, 21);
            this.cbxZespół.TabIndex = 63;
            // 
            // rbtWszystko
            // 
            this.rbtWszystko.AutoSize = true;
            this.rbtWszystko.Location = new System.Drawing.Point(144, 19);
            this.rbtWszystko.Name = "rbtWszystko";
            this.rbtWszystko.Size = new System.Drawing.Size(45, 17);
            this.rbtWszystko.TabIndex = 62;
            this.rbtWszystko.Text = "Oba";
            this.rbtWszystko.UseVisualStyleBackColor = true;
            this.rbtWszystko.CheckedChanged += new System.EventHandler(this.rbtWszystko_CheckedChanged);
            // 
            // rbtZespół
            // 
            this.rbtZespół.AutoSize = true;
            this.rbtZespół.Checked = true;
            this.rbtZespół.Location = new System.Drawing.Point(39, 19);
            this.rbtZespół.Name = "rbtZespół";
            this.rbtZespół.Size = new System.Drawing.Size(59, 17);
            this.rbtZespół.TabIndex = 60;
            this.rbtZespół.TabStop = true;
            this.rbtZespół.Text = "Zespół";
            this.rbtZespół.UseVisualStyleBackColor = true;
            this.rbtZespół.CheckedChanged += new System.EventHandler(this.rbtZespół_CheckedChanged);
            // 
            // rbtSolista
            // 
            this.rbtSolista.AutoSize = true;
            this.rbtSolista.Location = new System.Drawing.Point(236, 19);
            this.rbtSolista.Name = "rbtSolista";
            this.rbtSolista.Size = new System.Drawing.Size(56, 17);
            this.rbtSolista.TabIndex = 61;
            this.rbtSolista.Text = "Solista";
            this.rbtSolista.UseVisualStyleBackColor = true;
            this.rbtSolista.CheckedChanged += new System.EventHandler(this.rbtSolista_CheckedChanged);
            // 
            // btnUsuń
            // 
            this.btnUsuń.Location = new System.Drawing.Point(136, 206);
            this.btnUsuń.Name = "btnUsuń";
            this.btnUsuń.Size = new System.Drawing.Size(56, 23);
            this.btnUsuń.TabIndex = 68;
            this.btnUsuń.Text = "Usuń";
            this.btnUsuń.UseVisualStyleBackColor = true;
            this.btnUsuń.Click += new System.EventHandler(this.btnUsuń_Click);
            // 
            // cbxWybierzPiosenke
            // 
            this.cbxWybierzPiosenke.FormattingEnabled = true;
            this.cbxWybierzPiosenke.Location = new System.Drawing.Point(96, 6);
            this.cbxWybierzPiosenke.Name = "cbxWybierzPiosenke";
            this.cbxWybierzPiosenke.Size = new System.Drawing.Size(151, 21);
            this.cbxWybierzPiosenke.TabIndex = 67;
            this.cbxWybierzPiosenke.SelectedIndexChanged += new System.EventHandler(this.cbxWybierzPiosenke_SelectedIndexChanged);
            // 
            // btnPoprzedni
            // 
            this.btnPoprzedni.Location = new System.Drawing.Point(12, 191);
            this.btnPoprzedni.Name = "btnPoprzedni";
            this.btnPoprzedni.Size = new System.Drawing.Size(75, 23);
            this.btnPoprzedni.TabIndex = 66;
            this.btnPoprzedni.Text = "<Poprzedni<";
            this.btnPoprzedni.UseVisualStyleBackColor = true;
            this.btnPoprzedni.Click += new System.EventHandler(this.btnPoprzedni_Click);
            // 
            // btnNastępny
            // 
            this.btnNastępny.Location = new System.Drawing.Point(255, 191);
            this.btnNastępny.Name = "btnNastępny";
            this.btnNastępny.Size = new System.Drawing.Size(75, 23);
            this.btnNastępny.TabIndex = 65;
            this.btnNastępny.Text = ">Następny>";
            this.btnNastępny.UseVisualStyleBackColor = true;
            this.btnNastępny.Click += new System.EventHandler(this.btnNastępny_Click);
            // 
            // Zarządzaj_Piosenkami
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 242);
            this.Controls.Add(this.btnUsuń);
            this.Controls.Add(this.cbxWybierzPiosenke);
            this.Controls.Add(this.btnPoprzedni);
            this.Controls.Add(this.btnNastępny);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnDodaj);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxNazwa);
            this.Name = "Zarządzaj_Piosenkami";
            this.Text = "Zarządzanie Piosenkami";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudSekundy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinuty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudGodziny)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxNazwa;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown nudSekundy;
        private System.Windows.Forms.NumericUpDown nudMinuty;
        private System.Windows.Forms.NumericUpDown nudGodziny;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cbxSolista;
        private System.Windows.Forms.ComboBox cbxZespół;
        private System.Windows.Forms.RadioButton rbtWszystko;
        private System.Windows.Forms.RadioButton rbtZespół;
        private System.Windows.Forms.RadioButton rbtSolista;
        private System.Windows.Forms.Button btnUsuń;
        private System.Windows.Forms.ComboBox cbxWybierzPiosenke;
        private System.Windows.Forms.Button btnPoprzedni;
        private System.Windows.Forms.Button btnNastępny;
    }
}