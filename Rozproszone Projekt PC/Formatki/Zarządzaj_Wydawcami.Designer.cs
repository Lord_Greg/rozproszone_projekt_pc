﻿namespace Rozproszone_Projekt_PC.Formatki
{
    partial class Zarządzaj_Wydawcami
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbxNazwa = new System.Windows.Forms.TextBox();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnUsuń = new System.Windows.Forms.Button();
            this.cbxWybierzWydawcę = new System.Windows.Forms.ComboBox();
            this.btnPoprzedni = new System.Windows.Forms.Button();
            this.btnNastępny = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Nazwa Wydawcy";
            // 
            // tbxNazwa
            // 
            this.tbxNazwa.Location = new System.Drawing.Point(12, 63);
            this.tbxNazwa.Name = "tbxNazwa";
            this.tbxNazwa.Size = new System.Drawing.Size(155, 20);
            this.tbxNazwa.TabIndex = 15;
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(107, 101);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(75, 23);
            this.btnDodaj.TabIndex = 18;
            this.btnDodaj.Text = "Zapisz";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnUsuń
            // 
            this.btnUsuń.Location = new System.Drawing.Point(116, 130);
            this.btnUsuń.Name = "btnUsuń";
            this.btnUsuń.Size = new System.Drawing.Size(56, 23);
            this.btnUsuń.TabIndex = 39;
            this.btnUsuń.Text = "Usuń";
            this.btnUsuń.UseVisualStyleBackColor = true;
            this.btnUsuń.Click += new System.EventHandler(this.btnUsuń_Click);
            // 
            // cbxWybierzWydawcę
            // 
            this.cbxWybierzWydawcę.FormattingEnabled = true;
            this.cbxWybierzWydawcę.Location = new System.Drawing.Point(70, 12);
            this.cbxWybierzWydawcę.Name = "cbxWybierzWydawcę";
            this.cbxWybierzWydawcę.Size = new System.Drawing.Size(151, 21);
            this.cbxWybierzWydawcę.TabIndex = 38;
            this.cbxWybierzWydawcę.SelectedIndexChanged += new System.EventHandler(this.cbxWybierzWydawcę_SelectedIndexChanged);
            // 
            // btnPoprzedni
            // 
            this.btnPoprzedni.Location = new System.Drawing.Point(16, 118);
            this.btnPoprzedni.Name = "btnPoprzedni";
            this.btnPoprzedni.Size = new System.Drawing.Size(75, 23);
            this.btnPoprzedni.TabIndex = 37;
            this.btnPoprzedni.Text = "<Poprzedni<";
            this.btnPoprzedni.UseVisualStyleBackColor = true;
            this.btnPoprzedni.Click += new System.EventHandler(this.btnPoprzedni_Click);
            // 
            // btnNastępny
            // 
            this.btnNastępny.Location = new System.Drawing.Point(201, 118);
            this.btnNastępny.Name = "btnNastępny";
            this.btnNastępny.Size = new System.Drawing.Size(75, 23);
            this.btnNastępny.TabIndex = 36;
            this.btnNastępny.Text = ">Następny>";
            this.btnNastępny.UseVisualStyleBackColor = true;
            this.btnNastępny.Click += new System.EventHandler(this.btnNastępny_Click);
            // 
            // Zarządzaj_Wydawcami
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 163);
            this.Controls.Add(this.btnUsuń);
            this.Controls.Add(this.cbxWybierzWydawcę);
            this.Controls.Add(this.btnPoprzedni);
            this.Controls.Add(this.btnNastępny);
            this.Controls.Add(this.btnDodaj);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxNazwa);
            this.Name = "Zarządzaj_Wydawcami";
            this.Text = "Zarządzanie Wydawcami";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxNazwa;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnUsuń;
        private System.Windows.Forms.ComboBox cbxWybierzWydawcę;
        private System.Windows.Forms.Button btnPoprzedni;
        private System.Windows.Forms.Button btnNastępny;
    }
}