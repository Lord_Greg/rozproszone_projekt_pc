﻿namespace Rozproszone_Projekt_PC.Formatki
{
    partial class Zarządzaj_Użytkownikami
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxNazwaUżytkownika = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.mtbxHasłoNowego = new System.Windows.Forms.TextBox();
            this.mtbxHasłoNowegoPotwierdź = new System.Windows.Forms.TextBox();
            this.mtbxHasłoPotwierdzająceDodaj = new System.Windows.Forms.TextBox();
            this.btnDodajUżytkownika = new System.Windows.Forms.Button();
            this.tcZakładki = new System.Windows.Forms.TabControl();
            this.tpDodaj = new System.Windows.Forms.TabPage();
            this.tpUsuń = new System.Windows.Forms.TabPage();
            this.cbxWybierzUżytkownika = new System.Windows.Forms.ComboBox();
            this.btnUsuń = new System.Windows.Forms.Button();
            this.mtbxHasłoPotwierdzająceUsuń = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tcZakładki.SuspendLayout();
            this.tpDodaj.SuspendLayout();
            this.tpUsuń.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbxNazwaUżytkownika
            // 
            this.tbxNazwaUżytkownika.Location = new System.Drawing.Point(9, 28);
            this.tbxNazwaUżytkownika.Name = "tbxNazwaUżytkownika";
            this.tbxNazwaUżytkownika.Size = new System.Drawing.Size(155, 20);
            this.tbxNazwaUżytkownika.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "Hasło";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Powtórz Hasło";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "Hasło Potwierdzające (Twoje)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Nazwa Użytkownika";
            // 
            // mtbxHasłoNowego
            // 
            this.mtbxHasłoNowego.Location = new System.Drawing.Point(9, 67);
            this.mtbxHasłoNowego.Name = "mtbxHasłoNowego";
            this.mtbxHasłoNowego.PasswordChar = '*';
            this.mtbxHasłoNowego.Size = new System.Drawing.Size(155, 20);
            this.mtbxHasłoNowego.TabIndex = 35;
            // 
            // mtbxHasłoNowegoPotwierdź
            // 
            this.mtbxHasłoNowegoPotwierdź.Location = new System.Drawing.Point(9, 106);
            this.mtbxHasłoNowegoPotwierdź.Name = "mtbxHasłoNowegoPotwierdź";
            this.mtbxHasłoNowegoPotwierdź.PasswordChar = '*';
            this.mtbxHasłoNowegoPotwierdź.Size = new System.Drawing.Size(155, 20);
            this.mtbxHasłoNowegoPotwierdź.TabIndex = 36;
            // 
            // mtbxHasłoPotwierdzająceDodaj
            // 
            this.mtbxHasłoPotwierdzająceDodaj.Location = new System.Drawing.Point(9, 161);
            this.mtbxHasłoPotwierdzająceDodaj.Name = "mtbxHasłoPotwierdzająceDodaj";
            this.mtbxHasłoPotwierdzająceDodaj.PasswordChar = '*';
            this.mtbxHasłoPotwierdzająceDodaj.Size = new System.Drawing.Size(155, 20);
            this.mtbxHasłoPotwierdzająceDodaj.TabIndex = 37;
            // 
            // btnDodajUżytkownika
            // 
            this.btnDodajUżytkownika.Location = new System.Drawing.Point(102, 195);
            this.btnDodajUżytkownika.Name = "btnDodajUżytkownika";
            this.btnDodajUżytkownika.Size = new System.Drawing.Size(75, 23);
            this.btnDodajUżytkownika.TabIndex = 38;
            this.btnDodajUżytkownika.Text = "Dodaj";
            this.btnDodajUżytkownika.UseVisualStyleBackColor = true;
            this.btnDodajUżytkownika.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // tcZakładki
            // 
            this.tcZakładki.Controls.Add(this.tpDodaj);
            this.tcZakładki.Controls.Add(this.tpUsuń);
            this.tcZakładki.Location = new System.Drawing.Point(12, 12);
            this.tcZakładki.Name = "tcZakładki";
            this.tcZakładki.SelectedIndex = 0;
            this.tcZakładki.Size = new System.Drawing.Size(289, 255);
            this.tcZakładki.TabIndex = 39;
            this.tcZakładki.SelectedIndexChanged += new System.EventHandler(this.tcZakładki_SelectedIndexChanged);
            // 
            // tpDodaj
            // 
            this.tpDodaj.Controls.Add(this.label5);
            this.tpDodaj.Controls.Add(this.btnDodajUżytkownika);
            this.tpDodaj.Controls.Add(this.tbxNazwaUżytkownika);
            this.tpDodaj.Controls.Add(this.mtbxHasłoPotwierdzająceDodaj);
            this.tpDodaj.Controls.Add(this.mtbxHasłoNowegoPotwierdź);
            this.tpDodaj.Controls.Add(this.label2);
            this.tpDodaj.Controls.Add(this.label3);
            this.tpDodaj.Controls.Add(this.mtbxHasłoNowego);
            this.tpDodaj.Controls.Add(this.label4);
            this.tpDodaj.Location = new System.Drawing.Point(4, 22);
            this.tpDodaj.Name = "tpDodaj";
            this.tpDodaj.Padding = new System.Windows.Forms.Padding(3);
            this.tpDodaj.Size = new System.Drawing.Size(281, 229);
            this.tpDodaj.TabIndex = 0;
            this.tpDodaj.Text = "Dodaj";
            this.tpDodaj.UseVisualStyleBackColor = true;
            // 
            // tpUsuń
            // 
            this.tpUsuń.Controls.Add(this.cbxWybierzUżytkownika);
            this.tpUsuń.Controls.Add(this.btnUsuń);
            this.tpUsuń.Controls.Add(this.mtbxHasłoPotwierdzająceUsuń);
            this.tpUsuń.Controls.Add(this.label9);
            this.tpUsuń.Location = new System.Drawing.Point(4, 22);
            this.tpUsuń.Name = "tpUsuń";
            this.tpUsuń.Padding = new System.Windows.Forms.Padding(3);
            this.tpUsuń.Size = new System.Drawing.Size(281, 229);
            this.tpUsuń.TabIndex = 1;
            this.tpUsuń.Text = "Usuń";
            this.tpUsuń.UseVisualStyleBackColor = true;
            // 
            // cbxWybierzUżytkownika
            // 
            this.cbxWybierzUżytkownika.FormattingEnabled = true;
            this.cbxWybierzUżytkownika.Location = new System.Drawing.Point(61, 30);
            this.cbxWybierzUżytkownika.Name = "cbxWybierzUżytkownika";
            this.cbxWybierzUżytkownika.Size = new System.Drawing.Size(155, 21);
            this.cbxWybierzUżytkownika.TabIndex = 45;
            // 
            // btnUsuń
            // 
            this.btnUsuń.Location = new System.Drawing.Point(102, 192);
            this.btnUsuń.Name = "btnUsuń";
            this.btnUsuń.Size = new System.Drawing.Size(75, 23);
            this.btnUsuń.TabIndex = 43;
            this.btnUsuń.Text = "Usuń";
            this.btnUsuń.UseVisualStyleBackColor = true;
            this.btnUsuń.Click += new System.EventHandler(this.btnUsuń_Click);
            // 
            // mtbxHasłoPotwierdzająceUsuń
            // 
            this.mtbxHasłoPotwierdzająceUsuń.Location = new System.Drawing.Point(61, 162);
            this.mtbxHasłoPotwierdzająceUsuń.Name = "mtbxHasłoPotwierdzająceUsuń";
            this.mtbxHasłoPotwierdzająceUsuń.PasswordChar = '*';
            this.mtbxHasłoPotwierdzająceUsuń.Size = new System.Drawing.Size(155, 20);
            this.mtbxHasłoPotwierdzająceUsuń.TabIndex = 41;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(58, 146);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(149, 13);
            this.label9.TabIndex = 39;
            this.label9.Text = "Hasło Potwierdzające (Twoje)";
            // 
            // Zarządzaj_Użytkownikami
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(313, 279);
            this.Controls.Add(this.tcZakładki);
            this.Name = "Zarządzaj_Użytkownikami";
            this.Text = "Zarządzanie Użytkownikami";
            this.tcZakładki.ResumeLayout(false);
            this.tpDodaj.ResumeLayout(false);
            this.tpDodaj.PerformLayout();
            this.tpUsuń.ResumeLayout(false);
            this.tpUsuń.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox tbxNazwaUżytkownika;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox mtbxHasłoNowego;
        private System.Windows.Forms.TextBox mtbxHasłoNowegoPotwierdź;
        private System.Windows.Forms.TextBox mtbxHasłoPotwierdzająceDodaj;
        private System.Windows.Forms.Button btnDodajUżytkownika;
        private System.Windows.Forms.TabControl tcZakładki;
        private System.Windows.Forms.TabPage tpDodaj;
        private System.Windows.Forms.TabPage tpUsuń;
        private System.Windows.Forms.ComboBox cbxWybierzUżytkownika;
        private System.Windows.Forms.Button btnUsuń;
        private System.Windows.Forms.TextBox mtbxHasłoPotwierdzająceUsuń;
        private System.Windows.Forms.Label label9;
    }
}