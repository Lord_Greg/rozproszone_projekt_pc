﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rozproszone_Projekt_PC.Modele_Danych;

namespace Rozproszone_Projekt_PC.Formatki
{
    public partial class Zarządzaj_Piosenkami : Form
    {
        private List<Piosenka> listaPiosenek;
        private List<Zespół> listaZespołów;
        private List<Artysta> listaSolistów;

        public Zarządzaj_Piosenkami()
        {
            InitializeComponent();

            przeładujPiosenki();

            załadujSolistów();
            załadujZespoły();
            cbxSolista.SelectedIndex = -1;
            cbxZespół.SelectedIndex = -1;
            cbxSolista.Enabled = false;
            cbxZespół.Enabled = true;

            wyczyśćKontrolki();
            btnUsuń.Visible = false;
        }

        #region Zdarzenia Kontrolek

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            string idZespołu = "", idSolisty = "";

            if ( (rbtSolista.Checked || rbtWszystko.Checked) && cbxSolista.SelectedIndex > -1 )
            {
                idSolisty = listaSolistów.ElementAt(cbxSolista.SelectedIndex).id.ToString();
            }
            if ( (rbtZespół.Checked || rbtWszystko.Checked) && cbxZespół.SelectedIndex > -1 )
            {
                idZespołu = listaZespołów.ElementAt(cbxZespół.SelectedIndex).id.ToString();
            }

            DialogResult dlgCzyZapisać = MessageBox.Show("Czy na pewno chcesz zapisać zmianę?", "Zapisz Zmianę", MessageBoxButtons.YesNo);
            if (dlgCzyZapisać == DialogResult.Yes)
            {
                Piosenka piosenka = new Piosenka(((Piosenka)cbxWybierzPiosenke.SelectedItem).Id, tbxNazwa.Text, nudGodziny.Value.ToString(), nudMinuty.Value.ToString(), nudSekundy.Value.ToString(), idZespołu, idSolisty);

                if (Poprawności_Danych.czyPodanoPoprawneDane(piosenka))
                {
                    if (cbxWybierzPiosenke.SelectedValue.ToString() != "0")
                    {
                        //Zmodyfikuj Istniejący
                        try
                        {
                            piosenka.aktualizujWbazie();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Wystąpił błąd:\n" + ex.Message);
                        }
                    }
                    else
                    {
                        //Dodaj Nowy
                        try
                        {
                            piosenka.dodajNowyDoBazy();
                            wyczyśćKontrolki();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Wystąpił błąd:\n" + ex.Message);
                        }
                    }

                    przeładujPiosenki();
                }
            }
            else if (dlgCzyZapisać == DialogResult.No)
            {
                //nie zapisuj
            }
        }

        
        private void btnNastępny_Click(object sender, EventArgs e)
        {
            if (cbxWybierzPiosenke.SelectedIndex + 1 < cbxWybierzPiosenke.Items.Count)
                cbxWybierzPiosenke.SelectedIndex++;
            else
                cbxWybierzPiosenke.SelectedIndex = 0;
        }

        private void btnPoprzedni_Click(object sender, EventArgs e)
        {
            if (cbxWybierzPiosenke.SelectedIndex - 1 >= 0)
                cbxWybierzPiosenke.SelectedIndex--;
            else
                if (cbxWybierzPiosenke.Items.Count > 0) cbxWybierzPiosenke.SelectedIndex = cbxWybierzPiosenke.Items.Count - 1;
            else cbxWybierzPiosenke.SelectedIndex = 0;
        }

        private void btnUsuń_Click(object sender, EventArgs e)
        {
            DialogResult dlgCzyUsunąć = MessageBox.Show("Czy na pewno chcesz usunąć tą piosenkę?", "Usuń Piosenkę", MessageBoxButtons.YesNo);
            if (dlgCzyUsunąć == DialogResult.Yes)
            {
                ((Piosenka)cbxWybierzPiosenke.SelectedItem).usuńZbazy();
                przeładujPiosenki();
            }
            else if (dlgCzyUsunąć == DialogResult.No)
            {
                //nie usuwaj
            }
        }


        private void rbtZespół_CheckedChanged(object sender, EventArgs e)
        {
            zmieńStanComboboxów();
        }

        private void rbtWszystko_CheckedChanged(object sender, EventArgs e)
        {
            zmieńStanComboboxów();
        }

        private void rbtSolista_CheckedChanged(object sender, EventArgs e)
        {
            zmieńStanComboboxów();
        }


        private void cbxWybierzPiosenke_SelectedIndexChanged(object sender, EventArgs e)
        {
            wczytajDanePiosenki((Piosenka)cbxWybierzPiosenke.SelectedItem);

            if (cbxWybierzPiosenke.SelectedValue.ToString() == "0" || cbxWybierzPiosenke.SelectedValue.ToString() == "-1")
            {
                wyczyśćKontrolki();
                btnUsuń.Visible = false;
            }
            else
            {
                btnUsuń.Visible = true;
            }
        }

        #endregion


        private void zmieńStanComboboxów()
        {
            if (rbtZespół.Checked)
            {
                cbxZespół.Enabled = true;
                cbxSolista.Enabled = false;
                cbxSolista.SelectedIndex = -1;
            }
            else if (rbtSolista.Checked)
            {
                cbxZespół.Enabled = false;
                cbxSolista.Enabled = true;
                cbxZespół.SelectedIndex = -1;
            }
            else if (rbtWszystko.Checked)
            {
                cbxZespół.Enabled = true;
                cbxSolista.Enabled = true;
            }
        }


        private void załadujZespoły()
        {
            listaZespołów = new List<Zespół>();

            listaZespołów.AddRange(Zespół.wczytajWszystkie());

            listaZespołów.Sort((x, y) => x.nazwa.CompareTo(y.nazwa));

            listaZespołów.Insert(0, new Zespół(0, "", 2000, ""));

            cbxZespół.DataSource = listaZespołów;
            cbxZespół.DisplayMember = "Nazwa";
            cbxZespół.ValueMember = "Id";
        }

        private void załadujSolistów()
        {
            listaSolistów = new List<Artysta>();

            listaSolistów.AddRange(Artysta.wczytajWszystkich());

            listaSolistów.Sort((x, y) => (x.Nazwa).CompareTo(y.Nazwa));

            listaSolistów.Insert(0, new Artysta(0, "", "", "", ""));

            cbxSolista.DataSource = listaSolistów;
            cbxSolista.DisplayMember = "Nazwa";
            cbxSolista.ValueMember = "Id";
        }


        private void przeładujPiosenki()
        {
            listaPiosenek = new List<Piosenka>();
            listaPiosenek.AddRange(Piosenka.wczytajWszystkie());
            listaPiosenek.Add(new Piosenka("0", "<Nowa Piosenka>", "0", "0", "0", "", ""));
            listaPiosenek.Sort((x, y) => x.Tytuł.CompareTo(y.Tytuł));

            cbxWybierzPiosenke.DataSource = listaPiosenek;
            cbxWybierzPiosenke.DisplayMember = "Tytuł";
            cbxWybierzPiosenke.ValueMember = "Id";
        }


        private void wczytajDanePiosenki(Piosenka piosenka)
        {
            if (piosenka == null)
            {
                if (listaPiosenek.Count > 0) piosenka = listaPiosenek.ElementAt(0);
                else return;
            }

            tbxNazwa.Text = piosenka.tytuł;
            nudGodziny.Value = piosenka.długość.godziny;
            nudMinuty.Value = piosenka.długość.minuty;
            nudSekundy.Value = piosenka.długość.sekundy;

            if(piosenka.idZespołu == "0")
            {
                if(piosenka.idArtysty == "0")
                {
                    rbtWszystko.Checked = true;
                }
                else
                {
                    rbtSolista.Checked = true;
                }
            }
            else
            {
                if (piosenka.idArtysty == "0")
                {
                    rbtZespół.Checked = true;
                }
                else
                {
                    rbtWszystko.Checked = true;
                }
            }

            cbxZespół.SelectedValue = piosenka.idZespołu;
            cbxSolista.SelectedValue = piosenka.idArtysty;
        }


        private void wyczyśćKontrolki()
        {
            tbxNazwa.Text = "";
            nudGodziny.Value = 0;
            nudMinuty.Value = 0;
            nudSekundy.Value = 0;
        }

            }
}