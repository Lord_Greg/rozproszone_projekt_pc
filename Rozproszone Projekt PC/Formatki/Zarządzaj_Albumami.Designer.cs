﻿namespace Rozproszone_Projekt_PC.Formatki
{
    partial class Zarządzaj_Albumami
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnZapisz = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxNazwa = new System.Windows.Forms.TextBox();
            this.nudRok = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxWydawca = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.nudCena = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.nudIlośćNaStanie = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.nudIlośćSprzedanych = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.tbxKodKreskowy = new System.Windows.Forms.TextBox();
            this.dgvPiosenki = new System.Windows.Forms.DataGridView();
            this.dgvcNrŚcieżki = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcTytułPiosenki = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvWykonawcy = new System.Windows.Forms.DataGridView();
            this.dgvcWykonawca = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvGatunki = new System.Windows.Forms.DataGridView();
            this.dgvcGatunek = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvLokalizacje_Dział = new System.Windows.Forms.DataGridView();
            this.dgvcDziały = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.dgvLokalizacje_Pułki = new System.Windows.Forms.DataGridView();
            this.dgvcPułki = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.btnUsuń = new System.Windows.Forms.Button();
            this.cbxWybierzAlbum = new System.Windows.Forms.ComboBox();
            this.btnPoprzedni = new System.Windows.Forms.Button();
            this.btnNastępny = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudRok)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCena)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIlośćNaStanie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIlośćSprzedanych)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPiosenki)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWykonawcy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGatunki)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLokalizacje_Dział)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLokalizacje_Pułki)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnZapisz
            // 
            this.btnZapisz.Location = new System.Drawing.Point(137, 366);
            this.btnZapisz.Name = "btnZapisz";
            this.btnZapisz.Size = new System.Drawing.Size(75, 23);
            this.btnZapisz.TabIndex = 46;
            this.btnZapisz.Text = "Zapisz";
            this.btnZapisz.UseVisualStyleBackColor = true;
            this.btnZapisz.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 40;
            this.label1.Text = "Tytuł";
            // 
            // tbxNazwa
            // 
            this.tbxNazwa.Location = new System.Drawing.Point(32, 61);
            this.tbxNazwa.Name = "tbxNazwa";
            this.tbxNazwa.Size = new System.Drawing.Size(156, 20);
            this.tbxNazwa.TabIndex = 39;
            // 
            // nudRok
            // 
            this.nudRok.Location = new System.Drawing.Point(254, 61);
            this.nudRok.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudRok.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudRok.Name = "nudRok";
            this.nudRok.Size = new System.Drawing.Size(55, 20);
            this.nudRok.TabIndex = 47;
            this.nudRok.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(251, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 48;
            this.label2.Text = "Rok Wydania";
            // 
            // cbxWydawca
            // 
            this.cbxWydawca.FormattingEnabled = true;
            this.cbxWydawca.Location = new System.Drawing.Point(32, 109);
            this.cbxWydawca.Name = "cbxWydawca";
            this.cbxWydawca.Size = new System.Drawing.Size(156, 21);
            this.cbxWydawca.TabIndex = 49;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 50;
            this.label3.Text = "Wydawca";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(251, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 52;
            this.label4.Text = "Cena";
            // 
            // nudCena
            // 
            this.nudCena.DecimalPlaces = 2;
            this.nudCena.Location = new System.Drawing.Point(254, 110);
            this.nudCena.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.nudCena.Name = "nudCena";
            this.nudCena.Size = new System.Drawing.Size(55, 20);
            this.nudCena.TabIndex = 51;
            this.nudCena.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(251, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 54;
            this.label5.Text = "Ilość na Stanie";
            // 
            // nudIlośćNaStanie
            // 
            this.nudIlośćNaStanie.Location = new System.Drawing.Point(254, 164);
            this.nudIlośćNaStanie.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nudIlośćNaStanie.Name = "nudIlośćNaStanie";
            this.nudIlośćNaStanie.Size = new System.Drawing.Size(55, 20);
            this.nudIlośćNaStanie.TabIndex = 53;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(251, 196);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 13);
            this.label6.TabIndex = 56;
            this.label6.Text = "Ilość na Sprzedanych";
            // 
            // nudIlośćSprzedanych
            // 
            this.nudIlośćSprzedanych.Location = new System.Drawing.Point(254, 212);
            this.nudIlośćSprzedanych.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.nudIlośćSprzedanych.Name = "nudIlośćSprzedanych";
            this.nudIlośćSprzedanych.Size = new System.Drawing.Size(55, 20);
            this.nudIlośćSprzedanych.TabIndex = 55;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(29, 148);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 58;
            this.label7.Text = "Kod Kreskowy";
            // 
            // tbxKodKreskowy
            // 
            this.tbxKodKreskowy.Location = new System.Drawing.Point(32, 164);
            this.tbxKodKreskowy.Name = "tbxKodKreskowy";
            this.tbxKodKreskowy.Size = new System.Drawing.Size(156, 20);
            this.tbxKodKreskowy.TabIndex = 57;
            // 
            // dgvPiosenki
            // 
            this.dgvPiosenki.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPiosenki.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvcNrŚcieżki,
            this.dgvcTytułPiosenki});
            this.dgvPiosenki.Location = new System.Drawing.Point(366, 12);
            this.dgvPiosenki.Name = "dgvPiosenki";
            this.dgvPiosenki.Size = new System.Drawing.Size(315, 406);
            this.dgvPiosenki.TabIndex = 64;
            this.dgvPiosenki.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvPiosenki_RowsAdded);
            this.dgvPiosenki.MouseEnter += new System.EventHandler(this.dgvPiosenki_MouseEnter);
            // 
            // dgvcNrŚcieżki
            // 
            this.dgvcNrŚcieżki.HeaderText = "Numer Ścieżki";
            this.dgvcNrŚcieżki.Name = "dgvcNrŚcieżki";
            this.dgvcNrŚcieżki.Width = 45;
            // 
            // dgvcTytułPiosenki
            // 
            this.dgvcTytułPiosenki.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvcTytułPiosenki.HeaderText = "Tytuł Piosenki";
            this.dgvcTytułPiosenki.Name = "dgvcTytułPiosenki";
            // 
            // dgvWykonawcy
            // 
            this.dgvWykonawcy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWykonawcy.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvcWykonawca});
            this.dgvWykonawcy.Location = new System.Drawing.Point(32, 247);
            this.dgvWykonawcy.Name = "dgvWykonawcy";
            this.dgvWykonawcy.Size = new System.Drawing.Size(277, 107);
            this.dgvWykonawcy.TabIndex = 65;
            // 
            // dgvcWykonawca
            // 
            this.dgvcWykonawca.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvcWykonawca.HeaderText = "Wykonawca";
            this.dgvcWykonawca.Name = "dgvcWykonawca";
            // 
            // dgvGatunki
            // 
            this.dgvGatunki.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGatunki.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvcGatunek});
            this.dgvGatunki.Location = new System.Drawing.Point(705, 12);
            this.dgvGatunki.Name = "dgvGatunki";
            this.dgvGatunki.Size = new System.Drawing.Size(199, 123);
            this.dgvGatunki.TabIndex = 66;
            // 
            // dgvcGatunek
            // 
            this.dgvcGatunek.HeaderText = "Gatunki";
            this.dgvcGatunek.Name = "dgvcGatunek";
            this.dgvcGatunek.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvcGatunek.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dgvcGatunek.Width = 150;
            // 
            // dgvLokalizacje_Dział
            // 
            this.dgvLokalizacje_Dział.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLokalizacje_Dział.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvcDziały});
            this.dgvLokalizacje_Dział.Location = new System.Drawing.Point(6, 19);
            this.dgvLokalizacje_Dział.Name = "dgvLokalizacje_Dział";
            this.dgvLokalizacje_Dział.Size = new System.Drawing.Size(199, 115);
            this.dgvLokalizacje_Dział.TabIndex = 67;
            // 
            // dgvcDziały
            // 
            this.dgvcDziały.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvcDziały.HeaderText = "Działy";
            this.dgvcDziały.Name = "dgvcDziały";
            // 
            // dgvLokalizacje_Pułki
            // 
            this.dgvLokalizacje_Pułki.AllowUserToDeleteRows = false;
            this.dgvLokalizacje_Pułki.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLokalizacje_Pułki.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvcPułki});
            this.dgvLokalizacje_Pułki.Location = new System.Drawing.Point(6, 140);
            this.dgvLokalizacje_Pułki.Name = "dgvLokalizacje_Pułki";
            this.dgvLokalizacje_Pułki.Size = new System.Drawing.Size(199, 133);
            this.dgvLokalizacje_Pułki.TabIndex = 68;
            this.dgvLokalizacje_Pułki.MouseEnter += new System.EventHandler(this.dgvLokalizacje_Pułki_MouseEnter);
            // 
            // dgvcPułki
            // 
            this.dgvcPułki.HeaderText = "Pułki";
            this.dgvcPułki.Name = "dgvcPułki";
            this.dgvcPułki.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvcPułki.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dgvcPułki.Width = 150;
            // 
            // btnUsuń
            // 
            this.btnUsuń.Location = new System.Drawing.Point(146, 395);
            this.btnUsuń.Name = "btnUsuń";
            this.btnUsuń.Size = new System.Drawing.Size(56, 23);
            this.btnUsuń.TabIndex = 72;
            this.btnUsuń.Text = "Usuń";
            this.btnUsuń.UseVisualStyleBackColor = true;
            this.btnUsuń.Click += new System.EventHandler(this.btnUsuń_Click);
            // 
            // cbxWybierzAlbum
            // 
            this.cbxWybierzAlbum.FormattingEnabled = true;
            this.cbxWybierzAlbum.Location = new System.Drawing.Point(66, 12);
            this.cbxWybierzAlbum.Name = "cbxWybierzAlbum";
            this.cbxWybierzAlbum.Size = new System.Drawing.Size(208, 21);
            this.cbxWybierzAlbum.TabIndex = 71;
            this.cbxWybierzAlbum.SelectedIndexChanged += new System.EventHandler(this.cbxWybierzAlbum_SelectedIndexChanged);
            // 
            // btnPoprzedni
            // 
            this.btnPoprzedni.Location = new System.Drawing.Point(32, 381);
            this.btnPoprzedni.Name = "btnPoprzedni";
            this.btnPoprzedni.Size = new System.Drawing.Size(75, 23);
            this.btnPoprzedni.TabIndex = 70;
            this.btnPoprzedni.Text = "<Poprzedni<";
            this.btnPoprzedni.UseVisualStyleBackColor = true;
            this.btnPoprzedni.Click += new System.EventHandler(this.btnPoprzedni_Click);
            // 
            // btnNastępny
            // 
            this.btnNastępny.Location = new System.Drawing.Point(234, 381);
            this.btnNastępny.Name = "btnNastępny";
            this.btnNastępny.Size = new System.Drawing.Size(75, 23);
            this.btnNastępny.TabIndex = 69;
            this.btnNastępny.Text = ">Następny>";
            this.btnNastępny.UseVisualStyleBackColor = true;
            this.btnNastępny.Click += new System.EventHandler(this.btnNastępny_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvLokalizacje_Dział);
            this.groupBox1.Controls.Add(this.dgvLokalizacje_Pułki);
            this.groupBox1.Location = new System.Drawing.Point(699, 145);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(214, 281);
            this.groupBox1.TabIndex = 73;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Lokalizacje";
            // 
            // Zarządzaj_Albumami
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(921, 432);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnUsuń);
            this.Controls.Add(this.cbxWybierzAlbum);
            this.Controls.Add(this.btnPoprzedni);
            this.Controls.Add(this.btnNastępny);
            this.Controls.Add(this.dgvGatunki);
            this.Controls.Add(this.dgvWykonawcy);
            this.Controls.Add(this.dgvPiosenki);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbxKodKreskowy);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.nudIlośćSprzedanych);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nudIlośćNaStanie);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.nudCena);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbxWydawca);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nudRok);
            this.Controls.Add(this.btnZapisz);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxNazwa);
            this.Name = "Zarządzaj_Albumami";
            this.Text = "Zarządzanie Albumami";
            ((System.ComponentModel.ISupportInitialize)(this.nudRok)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCena)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIlośćNaStanie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIlośćSprzedanych)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPiosenki)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWykonawcy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGatunki)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLokalizacje_Dział)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLokalizacje_Pułki)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnZapisz;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxNazwa;
        private System.Windows.Forms.NumericUpDown nudRok;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbxWydawca;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudCena;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nudIlośćNaStanie;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudIlośćSprzedanych;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbxKodKreskowy;
        private System.Windows.Forms.DataGridView dgvPiosenki;
        private System.Windows.Forms.DataGridView dgvWykonawcy;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcWykonawca;
        private System.Windows.Forms.DataGridView dgvGatunki;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcGatunek;
        private System.Windows.Forms.DataGridView dgvLokalizacje_Dział;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcDziały;
        private System.Windows.Forms.DataGridView dgvLokalizacje_Pułki;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcPułki;
        private System.Windows.Forms.Button btnUsuń;
        private System.Windows.Forms.ComboBox cbxWybierzAlbum;
        private System.Windows.Forms.Button btnPoprzedni;
        private System.Windows.Forms.Button btnNastępny;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcNrŚcieżki;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcTytułPiosenki;
    }
}