﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rozproszone_Projekt_PC.Modele_Danych;

namespace Rozproszone_Projekt_PC.Formatki
{
    public partial class Ekran_Logowania : Form
    {
        bool czyPoprawniePołączonoZbazą;

        public Ekran_Logowania()
        {
            InitializeComponent();
            
            try
            {
                if(Użytkownik.wczytajWszystkich().Count < 1)
                {
                    Użytkownik domyślnyUżytkownik = new Użytkownik("admin", "cisco");
                    domyślnyUżytkownik.dodajNowyDoBazy();
                }
                czyPoprawniePołączonoZbazą = true;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Błąd krytyczny połączenia z bazą danych. Aplikacja musi zostać zamknięta.");
                czyPoprawniePołączonoZbazą = false;
            }
        }


        private void btnZaloguj_Click(object sender, EventArgs e)
        {
            Użytkownik użytkownik = new Użytkownik(tbxNazwaUżytkownika.Text, mtbxHasło.Text);

            if (tbxNazwaUżytkownika.Text != "")
            {
                if (mtbxHasło.Text != "")
                {
                    try
                    {
                        if (użytkownik.zaloguj())
                        {
                            Zmienne_Globalne.zalogowanyUżytkownik = new Użytkownik(użytkownik);
                            this.Hide();
                            Menu_Główne dlgMenu = new Menu_Główne();
                            dlgMenu.ShowDialog();
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Podano nieprawidłową kombinację.");
                            wyczyśćKontrolki();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Wystąpił błąd:\n" + ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Podaj hasło.");
                }
            }
            else
            {
                MessageBox.Show("Podaj nazwę użytkownika.");
            }
        }


        private void wyczyśćKontrolki()
        {
            tbxNazwaUżytkownika.Text = "";
            mtbxHasło.Text = "";
        }

        private void Ekran_Logowania_Load(object sender, EventArgs e)
        {
            if (!czyPoprawniePołączonoZbazą)
            {
                this.Close();
            }
        }
    }
}
