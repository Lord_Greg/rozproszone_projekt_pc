﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rozproszone_Projekt_PC.Modele_Danych;

namespace Rozproszone_Projekt_PC.Formatki
{
    public partial class Zarządzaj_Użytkownikami : Form
    {
        List<Użytkownik> listaUżytkowników;

        public Zarządzaj_Użytkownikami()
        {
            InitializeComponent();
        }

        #region Zdarzenia Kontrolek

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            Użytkownik użytkownik = new Użytkownik(tbxNazwaUżytkownika.Text, mtbxHasłoNowego.Text);

            if (Poprawności_Danych.czyPodanoPoprawneDane(użytkownik, mtbxHasłoNowegoPotwierdź.Text, mtbxHasłoPotwierdzająceDodaj.Text))
            {
                try
                {
                    użytkownik.dodajNowyDoBazy();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Wystąpił błąd:\n" + ex.Message);
                }

                wyczyśćKontrolki();
            }
        }

        private void btnUsuń_Click(object sender, EventArgs e)
        {
            DialogResult dlgCzyUsunąć = MessageBox.Show("Czy na pewno chcesz usunąć tego użytkownika?", "Usuń Użytkownika", MessageBoxButtons.YesNo);
            if (cbxWybierzUżytkownika.Items.Count > 0)
            {
                if (dlgCzyUsunąć == DialogResult.Yes)
                {
                    if (mtbxHasłoPotwierdzająceUsuń.Text == Zmienne_Globalne.zalogowanyUżytkownik.hasło)
                    {
                        ((Użytkownik)cbxWybierzUżytkownika.SelectedItem).usuńZbazy();
                        przeładujUżytkowników();
                    }
                    else
                    {
                        MessageBox.Show("Podano błędne hasło weryfikacyjne.");
                    }
                    wyczyśćKontrolki();
                }
                else if (dlgCzyUsunąć == DialogResult.No)
                {
                    //nie usuwaj
                }
            }
            else
            {
                MessageBox.Show("Brak użytkowników do skasowania.");
            }
        }


        private void tcZakładki_SelectedIndexChanged(object sender, EventArgs e)
        {
            wyczyśćKontrolki();

            if (tcZakładki.SelectedIndex == 1)
            {
                przeładujUżytkowników();
            }
        }

        #endregion


        private void wyczyśćKontrolki()
        {
            tbxNazwaUżytkownika.Text = "";
            mtbxHasłoNowego.Text = "";
            mtbxHasłoNowegoPotwierdź.Text = "";
            mtbxHasłoPotwierdzająceDodaj.Text = "";
            mtbxHasłoPotwierdzająceUsuń.Text = "";
        }

        private void przeładujUżytkowników()
        {
            listaUżytkowników = new List<Użytkownik>();
            listaUżytkowników.AddRange(Użytkownik.wczytajWszystkich());

            listaUżytkowników.Remove(listaUżytkowników.Find(x => x.nazwa == Zmienne_Globalne.zalogowanyUżytkownik.nazwa));

            listaUżytkowników.Sort((x, y) => x.nazwa.CompareTo(y.nazwa));

            cbxWybierzUżytkownika.DataSource = listaUżytkowników;
            cbxWybierzUżytkownika.DisplayMember = "Nazwa";
            cbxWybierzUżytkownika.ValueMember = "Nazwa";
        }


    }
}