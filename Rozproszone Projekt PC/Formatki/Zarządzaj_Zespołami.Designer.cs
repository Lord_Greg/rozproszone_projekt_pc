﻿namespace Rozproszone_Projekt_PC.Formatki
{
    partial class Zarządzaj_Zespołami
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvCzłonkowie = new System.Windows.Forms.DataGridView();
            this.dgvcCzłonkowie = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.btnZapisz = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxPochodzenie = new System.Windows.Forms.TextBox();
            this.tbxNazwa = new System.Windows.Forms.TextBox();
            this.nudRokZałożenia = new System.Windows.Forms.NumericUpDown();
            this.btnNastępny = new System.Windows.Forms.Button();
            this.btnPoprzedni = new System.Windows.Forms.Button();
            this.cbxWybierzZespół = new System.Windows.Forms.ComboBox();
            this.btnUsuń = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCzłonkowie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRokZałożenia)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvCzłonkowie
            // 
            this.dgvCzłonkowie.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCzłonkowie.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvcCzłonkowie});
            this.dgvCzłonkowie.Location = new System.Drawing.Point(189, 50);
            this.dgvCzłonkowie.Name = "dgvCzłonkowie";
            this.dgvCzłonkowie.Size = new System.Drawing.Size(195, 150);
            this.dgvCzłonkowie.TabIndex = 26;
            // 
            // dgvcCzłonkowie
            // 
            this.dgvcCzłonkowie.HeaderText = "Członkowie";
            this.dgvcCzłonkowie.Name = "dgvcCzłonkowie";
            this.dgvcCzłonkowie.Width = 150;
            // 
            // btnZapisz
            // 
            this.btnZapisz.Location = new System.Drawing.Point(166, 210);
            this.btnZapisz.Name = "btnZapisz";
            this.btnZapisz.Size = new System.Drawing.Size(72, 23);
            this.btnZapisz.TabIndex = 25;
            this.btnZapisz.Text = "Zapisz";
            this.btnZapisz.UseVisualStyleBackColor = true;
            this.btnZapisz.Click += new System.EventHandler(this.btnZapisz_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Pochodzenie";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Rok Założenia";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Nazwa Zespołu";
            // 
            // tbxPochodzenie
            // 
            this.tbxPochodzenie.Location = new System.Drawing.Point(17, 159);
            this.tbxPochodzenie.Name = "tbxPochodzenie";
            this.tbxPochodzenie.Size = new System.Drawing.Size(155, 20);
            this.tbxPochodzenie.TabIndex = 21;
            // 
            // tbxNazwa
            // 
            this.tbxNazwa.Location = new System.Drawing.Point(17, 66);
            this.tbxNazwa.Name = "tbxNazwa";
            this.tbxNazwa.Size = new System.Drawing.Size(155, 20);
            this.tbxNazwa.TabIndex = 19;
            // 
            // nudRokZałożenia
            // 
            this.nudRokZałożenia.Location = new System.Drawing.Point(17, 114);
            this.nudRokZałożenia.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nudRokZałożenia.Name = "nudRokZałożenia";
            this.nudRokZałożenia.Size = new System.Drawing.Size(52, 20);
            this.nudRokZałożenia.TabIndex = 27;
            this.nudRokZałożenia.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            // 
            // btnNastępny
            // 
            this.btnNastępny.Location = new System.Drawing.Point(309, 221);
            this.btnNastępny.Name = "btnNastępny";
            this.btnNastępny.Size = new System.Drawing.Size(75, 23);
            this.btnNastępny.TabIndex = 28;
            this.btnNastępny.Text = ">Następny>";
            this.btnNastępny.UseVisualStyleBackColor = true;
            this.btnNastępny.Click += new System.EventHandler(this.btnNastępny_Click);
            // 
            // btnPoprzedni
            // 
            this.btnPoprzedni.Location = new System.Drawing.Point(17, 221);
            this.btnPoprzedni.Name = "btnPoprzedni";
            this.btnPoprzedni.Size = new System.Drawing.Size(75, 23);
            this.btnPoprzedni.TabIndex = 29;
            this.btnPoprzedni.Text = "<Poprzedni<";
            this.btnPoprzedni.UseVisualStyleBackColor = true;
            this.btnPoprzedni.Click += new System.EventHandler(this.btnPoprzedni_Click);
            // 
            // cbxWybierzZespół
            // 
            this.cbxWybierzZespół.FormattingEnabled = true;
            this.cbxWybierzZespół.Location = new System.Drawing.Point(120, 16);
            this.cbxWybierzZespół.Name = "cbxWybierzZespół";
            this.cbxWybierzZespół.Size = new System.Drawing.Size(164, 21);
            this.cbxWybierzZespół.TabIndex = 30;
            this.cbxWybierzZespół.SelectedIndexChanged += new System.EventHandler(this.cbxWybierzZespół_SelectedIndexChanged);
            // 
            // btnUsuń
            // 
            this.btnUsuń.Location = new System.Drawing.Point(174, 236);
            this.btnUsuń.Name = "btnUsuń";
            this.btnUsuń.Size = new System.Drawing.Size(56, 23);
            this.btnUsuń.TabIndex = 31;
            this.btnUsuń.Text = "Usuń";
            this.btnUsuń.UseVisualStyleBackColor = true;
            this.btnUsuń.Click += new System.EventHandler(this.btnUsuń_Click);
            // 
            // Zarządzaj_Zespołami
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 269);
            this.Controls.Add(this.btnUsuń);
            this.Controls.Add(this.cbxWybierzZespół);
            this.Controls.Add(this.btnPoprzedni);
            this.Controls.Add(this.btnNastępny);
            this.Controls.Add(this.nudRokZałożenia);
            this.Controls.Add(this.dgvCzłonkowie);
            this.Controls.Add(this.btnZapisz);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxPochodzenie);
            this.Controls.Add(this.tbxNazwa);
            this.Name = "Zarządzaj_Zespołami";
            this.Text = "Zarządzaj Zespołami";
            ((System.ComponentModel.ISupportInitialize)(this.dgvCzłonkowie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudRokZałożenia)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCzłonkowie;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcCzłonkowie;
        private System.Windows.Forms.Button btnZapisz;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxPochodzenie;
        private System.Windows.Forms.TextBox tbxNazwa;
        private System.Windows.Forms.NumericUpDown nudRokZałożenia;
        private System.Windows.Forms.Button btnNastępny;
        private System.Windows.Forms.Button btnPoprzedni;
        private System.Windows.Forms.ComboBox cbxWybierzZespół;
        private System.Windows.Forms.Button btnUsuń;
    }
}