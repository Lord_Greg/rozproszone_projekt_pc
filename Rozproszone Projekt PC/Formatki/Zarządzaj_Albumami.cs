﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rozproszone_Projekt_PC.Modele_Danych;

namespace Rozproszone_Projekt_PC.Formatki
{
    public partial class Zarządzaj_Albumami : Form
    {
        private List<Album> listaAlbumów;
        private List<Zespół> listaZespołów;
        private List<Artysta> listaArtystów;
        private List<Wydawca> listaWydawców;
        private List<Wykonawca> listaWykonawców;
        private List<Piosenka> listaPiosenek;
        private List<Gatunek> listaGatunków;
        private List<Lokalizacja> listaLokalizacji;
        private List<string> działy;
        private List<Lokalizacja> listaPułek = new List<Lokalizacja>();


        public Zarządzaj_Albumami()
        {
            InitializeComponent();

            listaPiosenek = new List<Piosenka>();

            przeładujAlbumy();
            
            przeładujWydawców();
            przeładujWykonawców();
            przeładujGatunki();
            przeładujLokalizacje();

            cbxWydawca.SelectedIndex = 0;

            wyczyśćKontrolki();
            btnUsuń.Visible = false;

            dgvcTytułPiosenki.DataSource = listaPiosenek;
            dgvcTytułPiosenki.DisplayMember = "Tytuł";
            dgvcTytułPiosenki.ValueMember = "Id";
        }

        

        #region Zdarzenia Kontrolek

        private void cbxWybierzAlbum_SelectedIndexChanged(object sender, EventArgs e)
        {
            wczytajDaneAlbumu((Album)cbxWybierzAlbum.SelectedItem);

            if (cbxWybierzAlbum.SelectedIndex == 0)
            {
                wyczyśćKontrolki();
                btnUsuń.Visible = false;
            }
            else
            {
                btnUsuń.Visible = true;
            }
        }


        private void btnDodaj_Click(object sender, EventArgs e)
        {
            DialogResult dlgCzyZapisać = MessageBox.Show("Czy na pewno chcesz zapisać zmianę?", "Zapisz Zmianę", MessageBoxButtons.YesNo);
            if (dlgCzyZapisać == DialogResult.Yes)
            {
                List<Artysta> wybraniArtyści = new List<Artysta>();
                List<Zespół> wybraneZespoły = new List<Zespół>();
                List<Gatunek> wybraneGatunki = new List<Gatunek>();
                List<Ścieżka> wybraneŚcieżki = new List<Ścieżka>();
                List<Lokalizacja> wybraneLokalizacje = new List<Lokalizacja>();

                załadujPiosenkiDoGrida(true);

                //spisywanie Wykonawców
                for (int i = 0; i < dgvWykonawcy.RowCount - 1; i++)
                {
                    if (dgvWykonawcy.Rows[i].Cells[0].Value != null && dgvWykonawcy.Rows[i].Cells[0].Value.ToString() != "0" && dgvWykonawcy.Rows[i].Cells[0].Value.ToString() != "-1")
                    {
                        Wykonawca wykonawca = listaWykonawców.Find(x => x.index.ToString() == dgvWykonawcy.Rows[i].Cells[0].Value.ToString());
                        if (wykonawca.typ == Wykonawca.TYP_ARTYSTA)
                        {
                            wybraniArtyści.Add(listaArtystów.Find(x => x.id == wykonawca.id));
                        }
                        else if (wykonawca.typ == Wykonawca.TYP_ZESPÓŁ)
                        {
                            wybraneZespoły.Add(listaZespołów.Find(x => x.id == wykonawca.id));
                        }
                    }
                }

                //spisywanie Gatunków
                for (int i = 0; i < dgvGatunki.RowCount - 1; i++)
                {
                    if (dgvGatunki.Rows[i].Cells[0].Value != null && dgvGatunki.Rows[i].Cells[0].Value.ToString() != "0" && dgvGatunki.Rows[i].Cells[0].Value.ToString() != "-1")
                    {
                        wybraneGatunki.Add(listaGatunków.Find(x => x.nazwa == dgvGatunki.Rows[i].Cells[0].Value.ToString()));
                    }
                }

                //spisywanie Piosenek
                for (int i = 0; i < dgvPiosenki.RowCount - 1; i++)
                {
                    if (dgvPiosenki.Rows[i].Cells[1].Value != null && dgvPiosenki.Rows[i].Cells[1].Value.ToString() != "0" && dgvPiosenki.Rows[i].Cells[1].Value.ToString() != "-1")
                    {
                        wybraneŚcieżki.Add(new Ścieżka(Convert.ToInt32(dgvPiosenki.Rows[i].Cells[0].Value), listaPiosenek.Find(x => x.id == dgvPiosenki.Rows[i].Cells[1].Value.ToString())));
                    }
                }

                //spisywanie Lokalizacji
                for (int i = 0; i < dgvLokalizacje_Pułki.RowCount - 1; i++)
                {
                    if (dgvLokalizacje_Pułki.Rows[i].Cells[0].Value != null && dgvLokalizacje_Pułki.Rows[i].Cells[0].Value.ToString() != "0" && dgvLokalizacje_Pułki.Rows[i].Cells[0].Value.ToString() != "-1")
                    {
                        wybraneLokalizacje.Add(listaLokalizacji.Find(x => x.id.ToString() == dgvLokalizacje_Pułki.Rows[i].Cells[0].Value.ToString()));
                    }
                }

                Album album = new Album(((Album)cbxWybierzAlbum.SelectedItem).id, tbxNazwa.Text, Convert.ToInt32(nudRok.Value), Convert.ToInt32(cbxWydawca.SelectedValue), Convert.ToDouble(nudCena.Value), Convert.ToInt32(nudIlośćNaStanie.Value), Convert.ToInt32(nudIlośćSprzedanych.Value), tbxKodKreskowy.Text, wybraniArtyści, wybraneZespoły, wybraneŚcieżki, wybraneGatunki, wybraneLokalizacje);

                if (Poprawności_Danych.czyPodanoPoprawneDane(album))
                {
                    if (cbxWybierzAlbum.SelectedIndex > 0)
                    {
                        //Zmodyfikuj Istniejący
                        try
                        {
                            album.aktualizujWbazie();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Wystąpił błąd:\n" + ex.Message);
                        }
                    }
                    else
                    {
                        //Dodaj Nowy
                        try
                        {
                            album.dodajNowyDoBazy();
                            wyczyśćKontrolki();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Wystąpił błąd:\n" + ex.Message);
                        }
                    }

                    przeładujAlbumy();
                }
            }
            else if (dlgCzyZapisać == DialogResult.No)
            {
                //nie zapisuj
            }
        }

        private void btnUsuń_Click(object sender, EventArgs e)
        {
            DialogResult dlgCzyUsunąć = MessageBox.Show("Czy na pewno chcesz usunąć ten album?", "Usuń Album", MessageBoxButtons.YesNo);
            if (dlgCzyUsunąć == DialogResult.Yes)
            {
                ((Album)cbxWybierzAlbum.SelectedItem).usuńZbazy();
                przeładujAlbumy();
            }
            else if (dlgCzyUsunąć == DialogResult.No)
            {
                //nie usuwaj
            }
        }
        
        private void btnNastępny_Click(object sender, EventArgs e)
        {
            if (cbxWybierzAlbum.SelectedIndex + 1 < cbxWybierzAlbum.Items.Count)
                cbxWybierzAlbum.SelectedIndex++;
            else
                cbxWybierzAlbum.SelectedIndex = 0;
        }

        private void btnPoprzedni_Click(object sender, EventArgs e)
        {

            if (cbxWybierzAlbum.SelectedIndex - 1 >= 0)
                cbxWybierzAlbum.SelectedIndex--;
            else
                if (cbxWybierzAlbum.Items.Count > 0) cbxWybierzAlbum.SelectedIndex = cbxWybierzAlbum.Items.Count - 1;
            else cbxWybierzAlbum.SelectedIndex = 0;
        }


        private void dgvPiosenki_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (e.RowIndex > 0)
            {
                dgvPiosenki.Rows[e.RowIndex - 1].Cells[0].Value = (e.RowIndex).ToString();
            }
        }


        private void dgvPiosenki_MouseEnter(object sender, EventArgs e)
        {
            załadujPiosenkiDoGrida(true);
        }

        private void dgvLokalizacje_Pułki_MouseEnter(object sender, EventArgs e)
        {
            string[] zaznaczoneWartości = new string[dgvLokalizacje_Pułki.RowCount - 1];

            //Zapamiętywanie zaznaczonych wartości
            for (int i = 0; i < zaznaczoneWartości.GetLength(0); i++)
            {
                try
                {
                    zaznaczoneWartości[i] = dgvLokalizacje_Pułki.Rows[i].Cells[0].Value.ToString();
                }
                catch (Exception ex)
                {
                    zaznaczoneWartości[i] = "-1";
                }
                dgvLokalizacje_Pułki.Rows[i].Cells[0].Value = "-1";
            }

            listaPułek = new List<Lokalizacja>();

            listaPułek.Add(new Lokalizacja(-1, "", ""));

            List<string> użyteLokalizacje = new List<string>();

            //Dodawanie pułek do listy
            for (int i = 0; i < dgvLokalizacje_Dział.RowCount - 1; i++)
            {
                if (dgvLokalizacje_Dział.Rows[i].Cells[0].Value != null && dgvLokalizacje_Dział.Rows[i].Cells[0].Value.ToString() != "" && dgvLokalizacje_Dział.Rows[i].Cells[0].Value.ToString() != "-1" && !użyteLokalizacje.Exists(x => x == dgvLokalizacje_Dział.Rows[i].Cells[0].Value.ToString()))
                {
                    dodajPułkiDoListy(dgvLokalizacje_Dział.Rows[i].Cells[0].Value.ToString());
                    użyteLokalizacje.Add(dgvLokalizacje_Dział.Rows[i].Cells[0].Value.ToString());
                }
            }

            dgvcPułki.DataSource = listaPułek;
            dgvcPułki.DisplayMember = "Pułka";
            dgvcPułki.ValueMember = "Id";

            //Przywróć wcześniejsze zaznaczenia
            for (int i = 0; i < zaznaczoneWartości.GetLength(0); i++)
            {
                if (listaPułek.Exists(x => x.Id == zaznaczoneWartości[i]))
                {
                    dgvLokalizacje_Pułki.Rows[i].Cells[0].Value = zaznaczoneWartości[i];
                }
                else
                {
                    dgvLokalizacje_Pułki.Rows[i].Cells[0].Value = "-1";
                }
            }
        }

        #endregion


        private void wczytajDaneAlbumu(Album album)
        {
            if (album == null)
            {
                if (listaAlbumów.Count > 0) album = listaAlbumów.ElementAt(0);
                else return;
            }

            tbxNazwa.Text = album.nazwa;
            nudRok.Value = album.rokWydania;
            cbxWydawca.SelectedValue = album.idWydawcy.ToString();
            nudCena.Value = Convert.ToDecimal(album.cena);
            nudIlośćNaStanie.Value = album.ilośćNaStanie;
            nudIlośćSprzedanych.Value = album.ilośćSprzedanych;
            tbxKodKreskowy.Text = album.kodKreskowy;

            //Wykonawcy
            dgvWykonawcy.Rows.Clear();
            int i = 0;
            foreach (Zespół zespół in album.listaZespołów)
            {
                dgvWykonawcy.Rows.Add();
                dgvWykonawcy.Rows[i].Cells[0].Value = listaWykonawców.Find(x => x.typ == Wykonawca.TYP_ZESPÓŁ && x.id == zespół.id).Index;
                dodajPiosenkiDoListy(new Wykonawca(Wykonawca.TYP_ZESPÓŁ, zespół.id, zespół.Nazwa));
                i++;
            }
            foreach (Artysta artysta in album.listaArtystów)
            {
                dgvWykonawcy.Rows.Add();
                dgvWykonawcy.Rows[i].Cells[0].Value = listaWykonawców.Find(x => x.typ == Wykonawca.TYP_ARTYSTA && x.id == artysta.id).Index;
                dodajPiosenkiDoListy(new Wykonawca(Wykonawca.TYP_ARTYSTA, artysta.id, artysta.Nazwa));
                i++;
            }

            //Piosenki
            załadujPiosenkiDoGrida(false);

            dgvPiosenki.Rows.Clear();

            List<Piosenka> dozwolonePiosenki = new List<Piosenka>(załadujDozwolonePiosenki());

            if (album.listaŚcieżek.Count > 0)
            {
                for (int j = 0; j < album.listaŚcieżek.Max(x => x.nrŚcieżki); j++)
                {
                    dgvPiosenki.Rows.Add();
                    dgvPiosenki.Rows[j].Cells[0].Value = (j + 1).ToString();
                    if (album.listaŚcieżek.Exists(x => x.nrŚcieżki == j + 1))
                    {
                        string id = album.listaŚcieżek.Find(x => x.nrŚcieżki == j + 1).piosenka.Id;
                        if (dozwolonePiosenki.Exists(x => x.Id == id))
                        {
                            dgvPiosenki.Rows[j].Cells[1].Value = id;
                        }
                    }
                }
            }


            //Gatunki
            dgvGatunki.Rows.Clear();
            i = 0;
            foreach (Gatunek gatunek in album.listaGatunków)
            {
                dgvGatunki.Rows.Add();
                dgvGatunki.Rows[i].Cells[0].Value = listaGatunków.Find(x => x.nazwa == gatunek.nazwa).Nazwa;
                i++;
            }

            //Działy
            List<string> użyteLokalizacje = new List<string>();
            dgvLokalizacje_Dział.Rows.Clear();
            dgvLokalizacje_Pułki.Rows.Clear();
            i = 0;
            int licznikDziałów = 0;
            foreach (Lokalizacja lokalizacja in album.listaLokalizacji)
            {
                if (!użyteLokalizacje.Exists(x => x == lokalizacja.nazwaDziału))
                {
                    dgvLokalizacje_Dział.Rows.Add();
                    dgvLokalizacje_Dział.Rows[licznikDziałów].Cells[0].Value = lokalizacja.nazwaDziału;
                    listaPułek.Add(lokalizacja);
                    użyteLokalizacje.Add(lokalizacja.nazwaDziału);
                    licznikDziałów++;
                }
                dgvcPułki.DataSource = listaPułek;
                dgvcPułki.DisplayMember = "Pułka";
                dgvcPułki.ValueMember = "Id";

                dgvLokalizacje_Pułki.Rows.Add();
                dgvLokalizacje_Pułki.Rows[i].Cells[0].Value = lokalizacja.Id;
                i++;
            }
        }


        private void wyczyśćKontrolki()
        {
            tbxNazwa.Text = "";
            tbxKodKreskowy.Text = "";
            dgvWykonawcy.Rows.Clear();
            dgvPiosenki.Rows.Clear();
            dgvGatunki.Rows.Clear();
            dgvLokalizacje_Dział.Rows.Clear();
            dgvLokalizacje_Pułki.Rows.Clear();
        }


        private void przeładujAlbumy()
        {
            listaAlbumów = new List<Album>();
            listaAlbumów.AddRange(Album.wczytajWszystkie());
            listaAlbumów.Add(new Album(0, "<Nowy Album>"));
            listaAlbumów.Sort((x, y) => x.NazwaZwykonawcą.CompareTo(y.NazwaZwykonawcą));

            cbxWybierzAlbum.DataSource = listaAlbumów;
            cbxWybierzAlbum.DisplayMember = "NazwaZwykonawcą";
            cbxWybierzAlbum.ValueMember = "Id";
        }

        private void przeładujGatunki()
        {
            listaGatunków = new List<Gatunek>();

            listaGatunków.Add(new Gatunek(""));
            listaGatunków.AddRange(Gatunek.wczytajWszystkoZbazy());
            listaGatunków.Sort((x, y) => x.nazwa.CompareTo(y.nazwa));

            dgvcGatunek.DataSource = listaGatunków;
            dgvcGatunek.DisplayMember = "Nazwa";
            dgvcGatunek.ValueMember = "Nazwa";
        }

        private void przeładujLokalizacje()
        {
            listaLokalizacji = new List<Lokalizacja>();

            listaLokalizacji.Add(new Lokalizacja("", ""));
            listaLokalizacji.AddRange(Lokalizacja.wczytajWszystkoZbazy());
            
            działy = new List<string>();
            
            foreach(Lokalizacja lokalizacja in listaLokalizacji)
            {
                if (!działy.Exists(x => x == lokalizacja.nazwaDziału))
                {
                    działy.Add(lokalizacja.nazwaDziału);
                }
            }
            
            dgvcDziały.DataSource = działy;
        }

        private void przeładujWydawców()
        {

            listaWydawców = new List<Wydawca>();

            listaWydawców = Wydawca.wczytajWszystkich();

            listaWydawców.Add(new Wydawca(-1, ""));

            listaWydawców.Sort((x, y) => x.nazwa.CompareTo(y.nazwa));

            cbxWydawca.DataSource = listaWydawców;
            cbxWydawca.DisplayMember = "Nazwa";
            cbxWydawca.ValueMember = "Id";
        }

        private void przeładujWykonawców()
        {
            listaWykonawców = new List<Wykonawca>();

            listaZespołów = Zespół.wczytajWszystkie();
            listaArtystów = Artysta.wczytajWszystkich();

            foreach (Zespół element in listaZespołów)
            {
                listaWykonawców.Add(new Wykonawca(Wykonawca.TYP_ZESPÓŁ, element.id, element.nazwa));
            }

            foreach (Artysta element in listaArtystów)
            {
                listaWykonawców.Add(new Wykonawca(Wykonawca.TYP_ARTYSTA, element.id, element.Nazwa));
            }

            listaWykonawców.Add(new Wykonawca(Wykonawca.TYP_NIC, -1, ""));

            listaWykonawców.Sort((x, y) => x.nazwa.CompareTo(y.nazwa));

            int pozycjaNaLiście = 0;
            foreach (Wykonawca wykonawca in listaWykonawców)
            {
                wykonawca.index = pozycjaNaLiście;
                pozycjaNaLiście++;
            }

            dgvcWykonawca.DataSource = listaWykonawców;
            dgvcWykonawca.DisplayMember = "Nazwa";
            dgvcWykonawca.ValueMember = "Index";
        }


        private void dodajPiosenkiDoListy(Wykonawca wykonawca)
        {
            string warunek = "";

            if(wykonawca.typ == Wykonawca.TYP_ARTYSTA)
            {
                warunek += Obsługa_BD.Opisy.piosenki_id_artysty + "=" + wykonawca.id;
            }
            else if (wykonawca.typ == Wykonawca.TYP_ZESPÓŁ)
            {
                warunek += Obsługa_BD.Opisy.piosenki_id_zespołu + "=" + wykonawca.id;
            }
            else
            {
                return;
            }

            listaPiosenek.AddRange(Piosenka.wczytajZbazy(warunek));
        }

        private void dodajPiosenkiDoListy(Wykonawca wykonawca, List<Piosenka> listaPiosenek)
        {
            string warunek = "";

            if (wykonawca.typ == Wykonawca.TYP_ARTYSTA)
            {
                warunek += Obsługa_BD.Opisy.piosenki_id_artysty + "=" + wykonawca.id;
            }
            else if (wykonawca.typ == Wykonawca.TYP_ZESPÓŁ)
            {
                warunek += Obsługa_BD.Opisy.piosenki_id_zespołu + "=" + wykonawca.id;
            }
            else
            {
                return;
            }

            listaPiosenek.AddRange(Piosenka.wczytajZbazy(warunek));
        }

        private void dodajPułkiDoListy(string nazwaDziału)
        {
            if (nazwaDziału != "")
            {

                foreach (Lokalizacja lokalizacja in listaLokalizacji)
                {
                    if (lokalizacja.nazwaDziału == nazwaDziału)
                    {
                        listaPułek.Add(lokalizacja);
                    }
                }
            }

        }


        private void załadujPiosenkiDoGrida(bool zachowajObecne)
        {
            List<Piosenka> dozwolonePiosenki;

            //zapełnianie listy piosenek dla comboboxa
            dozwolonePiosenki = new List<Piosenka>(załadujDozwolonePiosenki());

            string[] zapamiętanePiosenki = new string[dgvPiosenki.RowCount - 1];

            //zapamiętywanie już wyświetlanych piosenek
            if (zachowajObecne)
            {
                for (int i = 0; i < dgvPiosenki.RowCount - 1; i++)
                {
                    if (dgvPiosenki.Rows[i].Cells[1].Value != null)
                    {
                        if (dozwolonePiosenki.Exists(x => x.Id == dgvPiosenki.Rows[i].Cells[1].Value.ToString()))
                        {
                            zapamiętanePiosenki[i] = dgvPiosenki.Rows[i].Cells[1].Value.ToString();
                        }
                        else
                        {
                            zapamiętanePiosenki[i] = "-1";
                        }
                    }
                    else
                    {
                        zapamiętanePiosenki[i] = "-1";
                    }
                }
            }

            //czyszczenie danych w gridzie
            dgvPiosenki.Rows.Clear();

            listaPiosenek = new List<Piosenka>(dozwolonePiosenki);
            listaPiosenek.Sort((x, y) => x.Tytuł.CompareTo(y.Tytuł));
            listaPiosenek.Insert(0, new Piosenka());

            dgvcTytułPiosenki.DataSource = listaPiosenek;
            dgvcTytułPiosenki.DisplayMember = "Tytuł";
            dgvcTytułPiosenki.ValueMember = "Id";

            if (zachowajObecne)
            {
                //przywracanie poprzednich piosenek
                for (int i = 0; i < zapamiętanePiosenki.GetLength(0); i++)
                {
                    dgvPiosenki.Rows.Add();
                    dgvPiosenki.Rows[i].Cells[0].Value = (i + 1).ToString();

                    if (listaPiosenek.Exists(x => x.Id == zapamiętanePiosenki[i]))
                    {
                        dgvPiosenki.Rows[i].Cells[1].Value = zapamiętanePiosenki[i];
                    }
                    else
                    {
                        dgvPiosenki.Rows[i].Cells[1].Value = "-1";
                    }
                }
            }

        }

        private List<Piosenka> załadujDozwolonePiosenki()
        {
            List<Piosenka> dozwolonePiosenki = new List<Piosenka>();
            List<string> użyciWykonawcy = new List<string>();

            for (int i = 0; i < dgvWykonawcy.RowCount - 1; i++)
            {
                if (dgvWykonawcy.Rows[i].Cells[0].Value != null)
                {
                    if (!użyciWykonawcy.Exists(x => x == dgvWykonawcy.Rows[i].Cells[0].Value.ToString()))
                    {
                        użyciWykonawcy.Add(dgvWykonawcy.Rows[i].Cells[0].Value.ToString());
                        dodajPiosenkiDoListy(listaWykonawców.Find(x => x.Index.ToString() == dgvWykonawcy.Rows[i].Cells[0].Value.ToString()), dozwolonePiosenki);
                    }
                }
            }

            return dozwolonePiosenki;
        }

        private bool czyWartośćIstnieje(string idPiosenki, List<Piosenka> listaPiosenek)
        {
            bool czyIstnieje = false;

            foreach(Piosenka piosenka in listaPiosenek)
            {
                if(piosenka.Id == idPiosenki)
                {
                    czyIstnieje = true;
                    break;
                }
            }

            return czyIstnieje;
        }

    }

    class Wykonawca
    {
        public static string TYP_ZESPÓŁ = "Zespół";
        public static string TYP_ARTYSTA = "Artysta";
        public static string TYP_NIC = "Nic";


        public int id, index;
        public string typ, nazwa;

        public string Nazwa
        {
            get { return nazwa; }
        }

        public int Index
        {
            get { return index;  }
        }


        public Wykonawca(string typ, int id, string nazwa)
        {
            this.typ = typ;
            this.id = id;
            this.nazwa = nazwa;
        }
    }
}
