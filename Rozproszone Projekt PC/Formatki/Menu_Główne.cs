﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using MySql.Data.Types;
using Rozproszone_Projekt_PC.Formatki;


namespace Rozproszone_Projekt_PC
{
    public partial class Menu_Główne : Form
    {
        public Menu_Główne()
        {
            InitializeComponent();
        }

        

        private void btnDodajArtyste_Click(object sender, EventArgs e)
        {
            Zarządzaj_Artystami frmDodajArtyste = new Zarządzaj_Artystami();
            frmDodajArtyste.Show();
        }

        private void btnDodajWydawce_Click(object sender, EventArgs e)
        {
            Zarządzaj_Wydawcami frmDodajWydawce = new Zarządzaj_Wydawcami();
            frmDodajWydawce.Show();
        }

        private void btnDodajGatunek_Click(object sender, EventArgs e)
        {
            Zarządzaj_Gatunkami frmDodajGatunek = new Zarządzaj_Gatunkami();
            frmDodajGatunek.Show();
        }

        private void btnDodajPiosenke_Click(object sender, EventArgs e)
        {
            Zarządzaj_Piosenkami frmDodajPiosenke = new Zarządzaj_Piosenkami();
            frmDodajPiosenke.Show();
        }

        private void btnDodajUżytkownika_Click(object sender, EventArgs e)
        {
            Zarządzaj_Użytkownikami frmDodajUżytkownika = new Zarządzaj_Użytkownikami();
            frmDodajUżytkownika.Show();
        }

        private void btnDodajAlbum_Click(object sender, EventArgs e)
        {
            Zarządzaj_Albumami frmDodajAlbum = new Zarządzaj_Albumami();
            frmDodajAlbum.Show();
        }

        private void btnDodajLokalizacje_Click(object sender, EventArgs e)
        {
            Zarządzaj_Lokalizacjami frmDodajLokalizacje = new Zarządzaj_Lokalizacjami();
            frmDodajLokalizacje.Show();
        }

        private void btnDodajZespoły_Click(object sender, EventArgs e)
        {
            Zarządzaj_Zespołami frmDodajZespoły = new Zarządzaj_Zespołami();
            frmDodajZespoły.Show();
        }

    }
}
