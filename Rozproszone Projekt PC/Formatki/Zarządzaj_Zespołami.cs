﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rozproszone_Projekt_PC.Modele_Danych;

namespace Rozproszone_Projekt_PC.Formatki
{
    public partial class Zarządzaj_Zespołami : Form
    {
        private List<Zespół> listaZespołów;
        private List<Artysta> listaArtystów;

        public Zarządzaj_Zespołami()
        {
            InitializeComponent();

            przeładujZespoły();
            przeładujArtystów();

            btnUsuń.Visible = false;
            tbxNazwa.Text = "";
        }


        #region Zdarzenia Kontrolek

        private void btnUsuń_Click(object sender, EventArgs e)
        {
            DialogResult dlgCzyUsunąć = MessageBox.Show("Czy na pewno chcesz usunąć ten zespół?", "Usuń Zespół", MessageBoxButtons.YesNo);
            if (dlgCzyUsunąć == DialogResult.Yes)
            {
                ((Zespół)cbxWybierzZespół.SelectedItem).usuńZbazy();
                przeładujZespoły();
            }
            else if (dlgCzyUsunąć == DialogResult.No)
            {
                //nie usuwaj
            }
        }

        private void btnZapisz_Click(object sender, EventArgs e)
        {
            DialogResult dlgCzyZapisać = MessageBox.Show("Czy na pewno chcesz zapisać zmianę?", "Zapisz Zmianę", MessageBoxButtons.YesNo);
            if (dlgCzyZapisać == DialogResult.Yes)
            {
                List<Artysta> wybraniCzłonkowie = new List<Artysta>();
                for (int i = 0; i < dgvCzłonkowie.RowCount - 1; i++)
                {
                    if (dgvCzłonkowie.Rows[i].Cells[0].Value != null && dgvCzłonkowie.Rows[i].Cells[0].Value.ToString() != "-1")
                        wybraniCzłonkowie.Add(listaArtystów.Find(x => x.Id == dgvCzłonkowie.Rows[i].Cells[0].Value.ToString()));
                }

                Zespół rozpatrywanyZespół = new Zespół(((Zespół)cbxWybierzZespół.SelectedItem).id, tbxNazwa.Text, nudRokZałożenia.Value.ToString(), tbxPochodzenie.Text, wybraniCzłonkowie);

                if (Poprawności_Danych.czyPodanoPoprawneDane(rozpatrywanyZespół))
                {
                    if (cbxWybierzZespół.SelectedValue.ToString() != "0")
                    {
                        //Zmodyfikuj Istniejący
                        try
                        {
                            rozpatrywanyZespół.aktualizujWbazie();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Wystąpił błąd:\n" + ex.Message);
                        }
                    }
                    else
                    {
                        //Dodaj Nowy
                        try
                        {
                            rozpatrywanyZespół.dodajNowyDoBazy();
                            tbxNazwa.Text = "";
                            nudRokZałożenia.Value = 2000;
                            tbxPochodzenie.Text = "";
                            dgvCzłonkowie.Rows.Clear();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Wystąpił błąd:\n" + ex.Message);
                        }
                    }

                    przeładujZespoły();
                }
            }
            else if (dlgCzyZapisać == DialogResult.No)
            {
                //nie zapisuj
            }
        }

        private void btnNastępny_Click(object sender, EventArgs e)
        {
            if (cbxWybierzZespół.SelectedIndex + 1 < cbxWybierzZespół.Items.Count)
                cbxWybierzZespół.SelectedIndex++;
            else
                cbxWybierzZespół.SelectedIndex = 0;
        }

        private void btnPoprzedni_Click(object sender, EventArgs e)
        {
            if (cbxWybierzZespół.SelectedIndex - 1 >= 0)
                cbxWybierzZespół.SelectedIndex--;
            else
                if (cbxWybierzZespół.Items.Count > 0) cbxWybierzZespół.SelectedIndex = cbxWybierzZespół.Items.Count - 1;
                else cbxWybierzZespół.SelectedIndex = 0;
        }


        private void cbxWybierzZespół_SelectedIndexChanged(object sender, EventArgs e)
        {
            wczytajDaneZespołu(listaZespołów.Find(x => x.Id == cbxWybierzZespół.SelectedValue.ToString()));

            if (cbxWybierzZespół.SelectedValue.ToString() == "0")
            {
                btnUsuń.Visible = false;
                tbxNazwa.Text = "";
            }
            else
            {
                btnUsuń.Visible = true;
            }
        }

        #endregion


        private void przeładujArtystów()
        {
            listaArtystów = new List<Artysta>();
            listaArtystów.AddRange(Artysta.wczytajWszystkich());
            listaArtystów.Add(new Artysta(-1, "", "", "", ""));
            listaArtystów.Sort((x, y) => x.Nazwa.CompareTo(y.Nazwa));

            dgvcCzłonkowie.DataSource = listaArtystów;
            dgvcCzłonkowie.DisplayMember = "Nazwa";
            dgvcCzłonkowie.ValueMember = "Id";
        }

        private void przeładujZespoły()
        {
            listaZespołów = new List<Zespół>();
            listaZespołów.AddRange(Zespół.wczytajWszystkie());
            listaZespołów.Sort((x, y) => x.nazwa.CompareTo(y.nazwa));
            listaZespołów.Insert(0, new Zespół(0, "<Nowy Zespół>", 2000, ""));

            cbxWybierzZespół.DataSource = listaZespołów;
            cbxWybierzZespół.DisplayMember = "Nazwa";
            cbxWybierzZespół.ValueMember = "Id";
        }

        private void wczytajDaneZespołu(Zespół zespół)
        {
            if (zespół == null)
            {
                if (listaZespołów.Count > 0) zespół = listaZespołów.ElementAt(0);
                else return;
            }

            dgvCzłonkowie.Rows.Clear();

            tbxNazwa.Text = zespół.nazwa;
            nudRokZałożenia.Value = zespół.rokZałożenia;
            tbxPochodzenie.Text = zespół.pochodzenie;

            int i = 0;
            foreach (Artysta członek in zespół.listaCzłonków)
            {
                dgvCzłonkowie.Rows.Add();
                dgvCzłonkowie.Rows[i].Cells[0].Value = członek.Id;
                i++;
            }
        }
        
    }
}
