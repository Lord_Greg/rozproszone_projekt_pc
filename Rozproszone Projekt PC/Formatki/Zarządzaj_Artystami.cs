﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rozproszone_Projekt_PC.Modele_Danych;

namespace Rozproszone_Projekt_PC.Formatki
{
    public partial class Zarządzaj_Artystami : Form
    {
        private List<Artysta> listaArtystów;


        public Zarządzaj_Artystami()
        {
            InitializeComponent();

            przeładujArtystów();
            wyczyśćKontrolki();
            btnUsuń.Visible = false;
        }


        #region Zdarzenia Kontrolek

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            DialogResult dlgCzyZapisać = MessageBox.Show("Czy na pewno chcesz zapisać zmianę?", "Zapisz Zmianę", MessageBoxButtons.YesNo);
            if (dlgCzyZapisać == DialogResult.Yes)
            {
                Artysta artysta = new Artysta( ((Artysta)cbxWybierzArtyste.SelectedItem).id, tbxImię.Text, tbxNazwisko.Text, tbxPseudonim.Text, tbxNarodowość.Text);



                if (Poprawności_Danych.czyPodanoPoprawneDane(artysta))
                {
                    if (cbxWybierzArtyste.SelectedValue.ToString() != "0")
                    {
                        //Zmodyfikuj Istniejący
                        try
                        {
                            artysta.aktualizujWbazie();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Wystąpił błąd:\n" + ex.Message);
                        }
                    }
                    else
                    {
                        //Dodaj Nowy
                        try
                        {
                            artysta.dodajNowyDoBazy();
                            wyczyśćKontrolki();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Wystąpił błąd:\n" + ex.Message);
                        }
                    }

                    przeładujArtystów();
                }
            }
            else if (dlgCzyZapisać == DialogResult.No)
            {
                //nie zapisuj
            }
        }

        private void btnNastępny_Click(object sender, EventArgs e)
        {
            if (cbxWybierzArtyste.SelectedIndex + 1 < cbxWybierzArtyste.Items.Count)
                cbxWybierzArtyste.SelectedIndex++;
            else
                cbxWybierzArtyste.SelectedIndex = 0;
        }

        private void btnPoprzedni_Click(object sender, EventArgs e)
        {
            if (cbxWybierzArtyste.SelectedIndex - 1 >= 0)
                cbxWybierzArtyste.SelectedIndex--;
            else
                if (cbxWybierzArtyste.Items.Count > 0) cbxWybierzArtyste.SelectedIndex = cbxWybierzArtyste.Items.Count - 1;
                else cbxWybierzArtyste.SelectedIndex = 0;
        }

        private void btnUsuń_Click(object sender, EventArgs e)
        {
            DialogResult dlgCzyUsunąć = MessageBox.Show("Czy na pewno chcesz usunąć tego artystę?", "Usuń Artystę", MessageBoxButtons.YesNo);
            if (dlgCzyUsunąć == DialogResult.Yes)
            {
                ((Artysta)cbxWybierzArtyste.SelectedItem).usuńZbazy();
                przeładujArtystów();
            }
            else if (dlgCzyUsunąć == DialogResult.No)
            {
                //nie usuwaj
            }
        }


        private void cbxWybierzArtyste_SelectedIndexChanged(object sender, EventArgs e)
        {
            wczytajDaneArtysty((Artysta)cbxWybierzArtyste.SelectedItem);

            if (cbxWybierzArtyste.SelectedValue.ToString() == "0")
            {
                wyczyśćKontrolki();
                btnUsuń.Visible = false;
            }
            else
            {
                btnUsuń.Visible = true;
            }
        }

        #endregion



        private void wyczyśćKontrolki()
        {
            tbxImię.Text = "";
            tbxNazwisko.Text = "";
            tbxPseudonim.Text = "";
            tbxNarodowość.Text = "";
        }


        private void przeładujArtystów()
        {
            listaArtystów = new List<Artysta>();
            listaArtystów.AddRange(Artysta.wczytajWszystkich());
            listaArtystów.Add(new Artysta(0, "", "<Nowy Artysta>", "", ""));
            listaArtystów.Sort((x, y) => x.Nazwa.CompareTo(y.Nazwa));

            cbxWybierzArtyste.DataSource = listaArtystów;
            cbxWybierzArtyste.DisplayMember = "Nazwa";
            cbxWybierzArtyste.ValueMember = "Id";
        }


        private void wczytajDaneArtysty(Artysta artysta)
        {
            if (artysta == null)
            {
                if (listaArtystów.Count > 0) artysta = listaArtystów.ElementAt(0);
                else return;
            }

            tbxImię.Text = artysta.imie;
            tbxNazwisko.Text = artysta.nazwisko;
            tbxPseudonim.Text = artysta.pseudonim;
            tbxNarodowość.Text = artysta.narodowość;
        }

    }
}
