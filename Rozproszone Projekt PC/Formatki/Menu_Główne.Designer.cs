﻿namespace Rozproszone_Projekt_PC
{
    partial class Menu_Główne
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnZarządzajArtystami = new System.Windows.Forms.Button();
            this.btnZarządzajWydawcami = new System.Windows.Forms.Button();
            this.btnZarządzajGatunkami = new System.Windows.Forms.Button();
            this.btnZarządzajPiosenkami = new System.Windows.Forms.Button();
            this.btnZarządzajUżytkownikami = new System.Windows.Forms.Button();
            this.btnZarządzajAlbumami = new System.Windows.Forms.Button();
            this.btnZarządzajLokalizacjami = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnPrzeglądajZespoły = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnZarządzajArtystami
            // 
            this.btnZarządzajArtystami.Location = new System.Drawing.Point(138, 31);
            this.btnZarządzajArtystami.Name = "btnZarządzajArtystami";
            this.btnZarządzajArtystami.Size = new System.Drawing.Size(77, 26);
            this.btnZarządzajArtystami.TabIndex = 1;
            this.btnZarządzajArtystami.Text = "Artyści";
            this.btnZarządzajArtystami.UseVisualStyleBackColor = true;
            this.btnZarządzajArtystami.Click += new System.EventHandler(this.btnDodajArtyste_Click);
            // 
            // btnZarządzajWydawcami
            // 
            this.btnZarządzajWydawcami.Location = new System.Drawing.Point(138, 139);
            this.btnZarządzajWydawcami.Name = "btnZarządzajWydawcami";
            this.btnZarządzajWydawcami.Size = new System.Drawing.Size(77, 26);
            this.btnZarządzajWydawcami.TabIndex = 3;
            this.btnZarządzajWydawcami.Text = "Wydawcy";
            this.btnZarządzajWydawcami.UseVisualStyleBackColor = true;
            this.btnZarządzajWydawcami.Click += new System.EventHandler(this.btnDodajWydawce_Click);
            // 
            // btnZarządzajGatunkami
            // 
            this.btnZarządzajGatunkami.Location = new System.Drawing.Point(247, 83);
            this.btnZarządzajGatunkami.Name = "btnZarządzajGatunkami";
            this.btnZarządzajGatunkami.Size = new System.Drawing.Size(77, 26);
            this.btnZarządzajGatunkami.TabIndex = 4;
            this.btnZarządzajGatunkami.Text = "Gatunki";
            this.btnZarządzajGatunkami.UseVisualStyleBackColor = true;
            this.btnZarządzajGatunkami.Click += new System.EventHandler(this.btnDodajGatunek_Click);
            // 
            // btnZarządzajPiosenkami
            // 
            this.btnZarządzajPiosenkami.Location = new System.Drawing.Point(26, 83);
            this.btnZarządzajPiosenkami.Name = "btnZarządzajPiosenkami";
            this.btnZarządzajPiosenkami.Size = new System.Drawing.Size(77, 27);
            this.btnZarządzajPiosenkami.TabIndex = 5;
            this.btnZarządzajPiosenkami.Text = "Piosenki";
            this.btnZarządzajPiosenkami.UseVisualStyleBackColor = true;
            this.btnZarządzajPiosenkami.Click += new System.EventHandler(this.btnDodajPiosenke_Click);
            // 
            // btnZarządzajUżytkownikami
            // 
            this.btnZarządzajUżytkownikami.Location = new System.Drawing.Point(247, 139);
            this.btnZarządzajUżytkownikami.Name = "btnZarządzajUżytkownikami";
            this.btnZarządzajUżytkownikami.Size = new System.Drawing.Size(77, 26);
            this.btnZarządzajUżytkownikami.TabIndex = 6;
            this.btnZarządzajUżytkownikami.Text = "Użytkownicy";
            this.btnZarządzajUżytkownikami.UseVisualStyleBackColor = true;
            this.btnZarządzajUżytkownikami.Click += new System.EventHandler(this.btnDodajUżytkownika_Click);
            // 
            // btnZarządzajAlbumami
            // 
            this.btnZarządzajAlbumami.Location = new System.Drawing.Point(138, 84);
            this.btnZarządzajAlbumami.Name = "btnZarządzajAlbumami";
            this.btnZarządzajAlbumami.Size = new System.Drawing.Size(77, 26);
            this.btnZarządzajAlbumami.TabIndex = 7;
            this.btnZarządzajAlbumami.Text = "Albumy";
            this.btnZarządzajAlbumami.UseVisualStyleBackColor = true;
            this.btnZarządzajAlbumami.Click += new System.EventHandler(this.btnDodajAlbum_Click);
            // 
            // btnZarządzajLokalizacjami
            // 
            this.btnZarządzajLokalizacjami.Location = new System.Drawing.Point(26, 138);
            this.btnZarządzajLokalizacjami.Name = "btnZarządzajLokalizacjami";
            this.btnZarządzajLokalizacjami.Size = new System.Drawing.Size(77, 27);
            this.btnZarządzajLokalizacjami.TabIndex = 8;
            this.btnZarządzajLokalizacjami.Text = "Lokalizacje";
            this.btnZarządzajLokalizacjami.UseVisualStyleBackColor = true;
            this.btnZarządzajLokalizacjami.Click += new System.EventHandler(this.btnDodajLokalizacje_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnPrzeglądajZespoły);
            this.groupBox1.Controls.Add(this.btnZarządzajUżytkownikami);
            this.groupBox1.Controls.Add(this.btnZarządzajAlbumami);
            this.groupBox1.Controls.Add(this.btnZarządzajLokalizacjami);
            this.groupBox1.Controls.Add(this.btnZarządzajArtystami);
            this.groupBox1.Controls.Add(this.btnZarządzajGatunkami);
            this.groupBox1.Controls.Add(this.btnZarządzajWydawcami);
            this.groupBox1.Controls.Add(this.btnZarządzajPiosenkami);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(351, 192);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Zarządzanie";
            // 
            // btnPrzeglądajZespoły
            // 
            this.btnPrzeglądajZespoły.Location = new System.Drawing.Point(26, 30);
            this.btnPrzeglądajZespoły.Name = "btnPrzeglądajZespoły";
            this.btnPrzeglądajZespoły.Size = new System.Drawing.Size(77, 27);
            this.btnPrzeglądajZespoły.TabIndex = 10;
            this.btnPrzeglądajZespoły.Text = "Zespoły";
            this.btnPrzeglądajZespoły.UseVisualStyleBackColor = true;
            this.btnPrzeglądajZespoły.Click += new System.EventHandler(this.btnDodajZespoły_Click);
            // 
            // Menu_Główne
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 215);
            this.Controls.Add(this.groupBox1);
            this.Name = "Menu_Główne";
            this.Text = "Menu Główne";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnZarządzajArtystami;
        private System.Windows.Forms.Button btnZarządzajWydawcami;
        private System.Windows.Forms.Button btnZarządzajGatunkami;
        private System.Windows.Forms.Button btnZarządzajPiosenkami;
        private System.Windows.Forms.Button btnZarządzajUżytkownikami;
        private System.Windows.Forms.Button btnZarządzajAlbumami;
        private System.Windows.Forms.Button btnZarządzajLokalizacjami;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnPrzeglądajZespoły;
    }
}

