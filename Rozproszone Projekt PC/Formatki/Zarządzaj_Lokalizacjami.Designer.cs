﻿namespace Rozproszone_Projekt_PC.Formatki
{
    partial class Zarządzaj_Lokalizacjami
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbxNazwaDziału = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbxSymbolPułki = new System.Windows.Forms.TextBox();
            this.btnDodaj = new System.Windows.Forms.Button();
            this.btnUsuń = new System.Windows.Forms.Button();
            this.cbxWybierzDział = new System.Windows.Forms.ComboBox();
            this.btnPoprzedni = new System.Windows.Forms.Button();
            this.btnNastępny = new System.Windows.Forms.Button();
            this.cbxWybierzPułke = new System.Windows.Forms.ComboBox();
            this.dgvAlbumy = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvcAlbum = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlbumy)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 61;
            this.label1.Text = "Nazwa";
            // 
            // tbxNazwaDziału
            // 
            this.tbxNazwaDziału.Location = new System.Drawing.Point(13, 69);
            this.tbxNazwaDziału.Name = "tbxNazwaDziału";
            this.tbxNazwaDziału.Size = new System.Drawing.Size(155, 20);
            this.tbxNazwaDziału.TabIndex = 60;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 68;
            this.label2.Text = "Symbol Pułki";
            // 
            // tbxSymbolPułki
            // 
            this.tbxSymbolPułki.Location = new System.Drawing.Point(14, 68);
            this.tbxSymbolPułki.Name = "tbxSymbolPułki";
            this.tbxSymbolPułki.Size = new System.Drawing.Size(170, 20);
            this.tbxSymbolPułki.TabIndex = 67;
            // 
            // btnDodaj
            // 
            this.btnDodaj.Location = new System.Drawing.Point(84, 231);
            this.btnDodaj.Name = "btnDodaj";
            this.btnDodaj.Size = new System.Drawing.Size(75, 23);
            this.btnDodaj.TabIndex = 69;
            this.btnDodaj.Text = "Zapisz";
            this.btnDodaj.UseVisualStyleBackColor = true;
            this.btnDodaj.Click += new System.EventHandler(this.btnDodaj_Click);
            // 
            // btnUsuń
            // 
            this.btnUsuń.Location = new System.Drawing.Point(94, 257);
            this.btnUsuń.Name = "btnUsuń";
            this.btnUsuń.Size = new System.Drawing.Size(56, 23);
            this.btnUsuń.TabIndex = 73;
            this.btnUsuń.Text = "Usuń";
            this.btnUsuń.UseVisualStyleBackColor = true;
            this.btnUsuń.Click += new System.EventHandler(this.btnUsuń_Click);
            // 
            // cbxWybierzDział
            // 
            this.cbxWybierzDział.FormattingEnabled = true;
            this.cbxWybierzDział.Location = new System.Drawing.Point(6, 19);
            this.cbxWybierzDział.Name = "cbxWybierzDział";
            this.cbxWybierzDział.Size = new System.Drawing.Size(158, 21);
            this.cbxWybierzDział.TabIndex = 72;
            this.cbxWybierzDział.SelectedIndexChanged += new System.EventHandler(this.cbxWybierzDział_SelectedIndexChanged);
            // 
            // btnPoprzedni
            // 
            this.btnPoprzedni.Location = new System.Drawing.Point(4, 244);
            this.btnPoprzedni.Name = "btnPoprzedni";
            this.btnPoprzedni.Size = new System.Drawing.Size(75, 23);
            this.btnPoprzedni.TabIndex = 71;
            this.btnPoprzedni.Text = "<Poprzedni<";
            this.btnPoprzedni.UseVisualStyleBackColor = true;
            this.btnPoprzedni.Click += new System.EventHandler(this.btnPoprzedni_Click);
            // 
            // btnNastępny
            // 
            this.btnNastępny.Location = new System.Drawing.Point(164, 244);
            this.btnNastępny.Name = "btnNastępny";
            this.btnNastępny.Size = new System.Drawing.Size(75, 23);
            this.btnNastępny.TabIndex = 70;
            this.btnNastępny.Text = ">Następny>";
            this.btnNastępny.UseVisualStyleBackColor = true;
            this.btnNastępny.Click += new System.EventHandler(this.btnNastępny_Click);
            // 
            // cbxWybierzPułke
            // 
            this.cbxWybierzPułke.FormattingEnabled = true;
            this.cbxWybierzPułke.Location = new System.Drawing.Point(9, 19);
            this.cbxWybierzPułke.Name = "cbxWybierzPułke";
            this.cbxWybierzPułke.Size = new System.Drawing.Size(160, 21);
            this.cbxWybierzPułke.TabIndex = 74;
            this.cbxWybierzPułke.SelectedIndexChanged += new System.EventHandler(this.cbxWybierzPułke_SelectedIndexChanged);
            // 
            // dgvAlbumy
            // 
            this.dgvAlbumy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAlbumy.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvcAlbum});
            this.dgvAlbumy.Location = new System.Drawing.Point(244, 18);
            this.dgvAlbumy.Name = "dgvAlbumy";
            this.dgvAlbumy.Size = new System.Drawing.Size(223, 262);
            this.dgvAlbumy.TabIndex = 75;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.cbxWybierzDział);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbxNazwaDziału);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(211, 197);
            this.groupBox1.TabIndex = 76;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dział";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbxWybierzPułke);
            this.groupBox2.Controls.Add(this.tbxSymbolPułki);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(6, 97);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(196, 95);
            this.groupBox2.TabIndex = 77;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pułka";
            // 
            // dgvcAlbum
            // 
            this.dgvcAlbum.HeaderText = "Albumy";
            this.dgvcAlbum.Name = "dgvcAlbum";
            this.dgvcAlbum.Width = 180;
            // 
            // Zarządzaj_Lokalizacjami
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 293);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgvAlbumy);
            this.Controls.Add(this.btnUsuń);
            this.Controls.Add(this.btnPoprzedni);
            this.Controls.Add(this.btnNastępny);
            this.Controls.Add(this.btnDodaj);
            this.Name = "Zarządzaj_Lokalizacjami";
            this.Text = "Zarządzanie Lokalizacjami";
            ((System.ComponentModel.ISupportInitialize)(this.dgvAlbumy)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxNazwaDziału;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbxSymbolPułki;
        private System.Windows.Forms.Button btnDodaj;
        private System.Windows.Forms.Button btnUsuń;
        private System.Windows.Forms.ComboBox cbxWybierzDział;
        private System.Windows.Forms.Button btnPoprzedni;
        private System.Windows.Forms.Button btnNastępny;
        private System.Windows.Forms.ComboBox cbxWybierzPułke;
        private System.Windows.Forms.DataGridView dgvAlbumy;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridViewComboBoxColumn dgvcAlbum;
    }
}