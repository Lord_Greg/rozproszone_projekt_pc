﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rozproszone_Projekt_PC.Modele_Danych;

namespace Rozproszone_Projekt_PC.Formatki
{
    public partial class Zarządzaj_Wydawcami : Form
    {
        private List<Wydawca> listaWydawców;


        public Zarządzaj_Wydawcami()
        {
            InitializeComponent();

            przeładujWydawców();
            wyczyśćKontrolki();
            btnUsuń.Visible = false;
        }


        #region Zdarzenia Kontrolek

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            DialogResult dlgCzyZapisać = MessageBox.Show("Czy na pewno chcesz zapisać zmianę?", "Zapisz Zmianę", MessageBoxButtons.YesNo);
            if (dlgCzyZapisać == DialogResult.Yes)
            {
                Wydawca wydawca = new Wydawca( ((Wydawca)cbxWybierzWydawcę.SelectedItem).id, tbxNazwa.Text);



                if (Poprawności_Danych.czyPodanoPoprawneDane(wydawca))
                {
                    if (cbxWybierzWydawcę.SelectedValue.ToString() != "0")
                    {
                        //Zmodyfikuj Istniejący
                        try
                        {
                            wydawca.aktualizujWbazie();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Wystąpił błąd:\n" + ex.Message);
                        }
                    }
                    else
                    {
                        //Dodaj Nowy
                        try
                        {
                            wydawca.dodajNowyDoBazy();
                            wyczyśćKontrolki();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Wystąpił błąd:\n" + ex.Message);
                        }

                    }

                    przeładujWydawców();
                }
            }
            else if (dlgCzyZapisać == DialogResult.No)
            {
                //nie zapisuj
            }
        }

        private void btnPoprzedni_Click(object sender, EventArgs e)
        {
            if (cbxWybierzWydawcę.SelectedIndex - 1 >= 0)
                cbxWybierzWydawcę.SelectedIndex--;
            else
                if (cbxWybierzWydawcę.Items.Count > 0) cbxWybierzWydawcę.SelectedIndex = cbxWybierzWydawcę.Items.Count - 1;
            else cbxWybierzWydawcę.SelectedIndex = 0;
        }

        private void btnNastępny_Click(object sender, EventArgs e)
        {
            if (cbxWybierzWydawcę.SelectedIndex + 1 < cbxWybierzWydawcę.Items.Count)
                cbxWybierzWydawcę.SelectedIndex++;
            else
                cbxWybierzWydawcę.SelectedIndex = 0;
        }

        private void btnUsuń_Click(object sender, EventArgs e)
        {
            DialogResult dlgCzyUsunąć = MessageBox.Show("Czy na pewno chcesz usunąć tego wydawcę?", "Usuń Wydawcę", MessageBoxButtons.YesNo);
            if (dlgCzyUsunąć == DialogResult.Yes)
            {
                ((Wydawca)cbxWybierzWydawcę.SelectedItem).usuńZbazy();
                przeładujWydawców();
            }
            else if (dlgCzyUsunąć == DialogResult.No)
            {
                //nie usuwaj
            }
        }


        private void cbxWybierzWydawcę_SelectedIndexChanged(object sender, EventArgs e)
        {
            wczytajDaneArtysty((Wydawca)cbxWybierzWydawcę.SelectedItem);

            if (cbxWybierzWydawcę.SelectedValue.ToString() == "0")
            {
                wyczyśćKontrolki();
                btnUsuń.Visible = false;
            }
            else
            {
                btnUsuń.Visible = true;
            }
        }

        #endregion


        private void wyczyśćKontrolki()
        {
            tbxNazwa.Text = "";
        }


        private void przeładujWydawców()
        {
            listaWydawców = new List<Wydawca>();
            listaWydawców.AddRange(Wydawca.wczytajWszystkich());
            listaWydawców.Add(new Wydawca(0, "<Nowy Wydawca>"));
            listaWydawców.Sort((x, y) => x.Nazwa.CompareTo(y.Nazwa));

            cbxWybierzWydawcę.DataSource = listaWydawców;
            cbxWybierzWydawcę.DisplayMember = "Nazwa";
            cbxWybierzWydawcę.ValueMember = "Id";
        }


        private void wczytajDaneArtysty(Wydawca wydawca)
        {
            if (wydawca == null)
            {
                if (listaWydawców.Count > 0) wydawca = listaWydawców.ElementAt(0);
                else return;
            }

            tbxNazwa.Text = wydawca.nazwa;
        }
    }
}
