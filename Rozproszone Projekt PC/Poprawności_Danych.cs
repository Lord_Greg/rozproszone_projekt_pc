﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Rozproszone_Projekt_PC.Modele_Danych;

namespace Rozproszone_Projekt_PC
{
    class Poprawności_Danych
    {
        public static bool czyPodanoPoprawneDane(Zespół zespół)
        {
            bool czyPoprawnie;

            if (zespół.nazwa == "")
            {
                MessageBox.Show("Podaj nazwę zespołu.");
                czyPoprawnie = false;
            }
            else
            {
                czyPoprawnie = true;
            }

            return czyPoprawnie;
        }

        public static bool czyPodanoPoprawneDane(Artysta artysta)
        {
            bool czyPoprawnie;

            if (artysta.imie == "" && artysta.nazwisko == "" && artysta.pseudonim == "")
            {
                MessageBox.Show("Podaj jakieś dane.");
                czyPoprawnie = false;
            }
            else
            {
                czyPoprawnie = true;
            }

            return czyPoprawnie;
        }

        public static bool czyPodanoPoprawneDane(Wydawca wydawca)
        {
            bool czyPoprawnie;

            if (wydawca.nazwa == "")
            {
                MessageBox.Show("Podaj nazwę wydawcy.");
                czyPoprawnie = false;
            }
            else
            {
                czyPoprawnie = true;
            }

            return czyPoprawnie;
        }

        public static bool czyPodanoPoprawneDane(Piosenka piosenka)
        {
            bool czyPoprawnie;

            if (piosenka.tytuł == "")
            {
                MessageBox.Show("Podaj tytuł piosenki.");
                czyPoprawnie = false;
            }
            else
            {
                czyPoprawnie = true;
            }

            return czyPoprawnie;
        }

        public static bool czyPodanoPoprawneDane(Lokalizacja lokalizacja)
        {
            bool czyPoprawnie;

            if (lokalizacja.nazwaDziału == "")
            {
                MessageBox.Show("Podaj nazwę działu.");
                czyPoprawnie = false;
            }
            else if (lokalizacja.symbolPułki == "")
            {
                MessageBox.Show("Podaj symbol pułki.");
                czyPoprawnie = false;
            }
            else
            {
                czyPoprawnie = true;
            }

            return czyPoprawnie;
        }

        public static bool czyPodanoPoprawneDane(Gatunek gatunek)
        {
            bool czyPoprawnie;

            if (gatunek.nazwa == "")
            {
                MessageBox.Show("Podaj nazwę gatunku.");
                czyPoprawnie = false;
            }
            else
            {
                czyPoprawnie = true;
            }

            return czyPoprawnie;
        }

        public static bool czyPodanoPoprawneDane(Album album)
        {
            bool czyPoprawnie;

            if (album.nazwa == "")
            {
                MessageBox.Show("Podaj nazwę albumu.");
                czyPoprawnie = false;
            }
            else
            {
                czyPoprawnie = true;
            }

            return czyPoprawnie;
        }

        public static bool czyPodanoPoprawneDane(Użytkownik użytkownik, string powtórzoneHasło, string hasłoWeryfikacyjne)
        {
            bool czyPoprawnie;

            if (użytkownik.nazwa == "")
            {
                MessageBox.Show("Podaj nazwę użytkownika.");
                czyPoprawnie = false;
            }
            else if (użytkownik.hasło == "" || powtórzoneHasło == "" || hasłoWeryfikacyjne == "")
            {
                MessageBox.Show("Podaj hasło.");
                czyPoprawnie = false;
            }
            else if (użytkownik.hasło.Length < 5)
            {
                MessageBox.Show("Hasło za krótkie (min. 5 znaków).");
                czyPoprawnie = false;
            }
            else if (użytkownik.hasło != powtórzoneHasło)
            {
                MessageBox.Show("Podane hasła nie są takie same.");
                czyPoprawnie = false;
            }
            else if (hasłoWeryfikacyjne != Zmienne_Globalne.zalogowanyUżytkownik.hasło)
            {
                MessageBox.Show("Podne hasło weryfikacyjne jest nieprawidłowe.");
                czyPoprawnie = false;
            }
            else
            {
                czyPoprawnie = true;
            }

            return czyPoprawnie;
        }

    }
}
