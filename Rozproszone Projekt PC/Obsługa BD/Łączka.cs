﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using MySql.Data.Types;
using System.Windows.Forms;

namespace Rozproszone_Projekt_PC.Obsługa_BD
{
    class Łączka
    {

        public static string adresSerwera = "192.168.43.81",
                             nazwaUżytkownika = "root",
                             nazwaBazy = "mydb";

        public static string strPolacz = "server=" + adresSerwera + 
                                         ";user=" + nazwaUżytkownika + 
                                         ";database=" + nazwaBazy +
                                         ";DefaultTableCacheAge=30;charset=utf8";


        public MySqlConnection połączenie;


        public void połącz()
        {
            połączenie = new MySqlConnection(strPolacz);
            try
            {
                połączenie.Open();
            }
            catch (Exception e)
            {
                MessageBox.Show("Wystąpił problem z połączeniem do bazy danych:\n" + e.Message);
            }
        }

        public void rozłącz()
        {
            try
            {
                połączenie.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Wystąpił problem z rozłączeniem z bazy danych:\n" + e.Message);
            }
        }

        public void wykonajPolecenieBezzwrotne(string polecenieSQL)
        {
            this.połącz();

            MySqlCommand komenda = new MySqlCommand(polecenieSQL, this.połączenie);
            komenda.ExecuteNonQuery();

            this.rozłącz();
        }

        public MySqlDataReader wykonajPolecenieZwrotne(string polecenieSQL)
        {
            MySqlCommand cmd_pk = new MySqlCommand(polecenieSQL, połączenie);
            MySqlDataReader kursor;

            kursor = cmd_pk.ExecuteReader();

            return kursor;
        }

    }
}
