﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rozproszone_Projekt_PC.Obsługa_BD
{
    class Opisy
    {
        public static string
            tab_Albumy = "Albumy",
            albumy_id_albumu = "id_albumu",
            albumy_nazwa = "nazwa",
            albumy_rok_wydania = "rok_wydania",
            albumy_id_wydawcy = "id_wydawcy",
            albumy_cena = "cena",
            albumy_ilość_na_stanie = "ilosc_na_stanie",
            albumy_ilość_sprzedanych = "ilosc_sprzedanych",
            albumy_kod_kreskowy = "kod_kreskowy",

            tab_Artyści = "Artysci",
            artyści_id_artysty = "id_artysty",
            artyści_imię = "imie",
            artyści_nazwisko = "nazwisko",
            artyści_pseudonim = "pseudonim",
            artyści_narodowość = "narodowosc",

            tab_Artyści_w_Albumach = "Artysci_w_Albumach",
            artyści_w_albumach_id_artysty = "id_artysty",
            artyści_w_albumach_id_albumu = "id_albumu",

            tab_Artyści_w_Zespołach = "Artysci_w_Zespolach",
            artyści_w_zespołach_id_zespołu = "id_zespolu",
            artyści_w_zespołach_id_artysty = "id_artysty",

            tab_Gatunki = "Gatunki",
            gatunki_nazwa = "nazwa",

            tab_Gatunki_Albumów = "Gatunki_Albumow",
            gatunki_albumów_nazwa_gatunku = "nazwa_gatunku",
            gatunki_albumów_id_albumu = "id_albumu",

            tab_Piosenki = "Piosenki",
            piosenki_id_piosenki = "id_piosenki",
            piosenki_tytuł = "tytul",
            piosenki_długość = "dlugosc",
            piosenki_id_zespołu = "id_zespolu",
            piosenki_id_artysty = "id_artysty",

            tab_Piosenki_w_Albumach = "Piosenki_w_Albumach",
            piosenki_w_albumach_id_piosenki = "id_piosenki",
            piosenki_w_albumach_id_albumu = "id_albumu",
            piosenki_w_albumach_numer_ścieżki = "numer_sciezki",

            tab_Użytkownicy = "Uzytkownicy",
            użytkownicy_nazwa = "nazwa",
            użytkownicy_hasło = "haslo",

            tab_Wydawcy = "Wydawcy",
            wydawcy_id_wydawcy = "id_wydawcy",
            wydawcy_nazwa = "nazwa",

            tab_Zespoły = "Zespoly",
            zespoły_id_zespołu = "id_zespolu",
            zespoły_nazwa = "nazwa",
            zespoły_rok_założenia = "rok_zalozenia",
            zespoły_pochodzenie = "pochodzenie",

            tab_Zespoły_w_Albumach = "Zespoly_w_Albumach",
            zespoły_w_albumach_id_zespołu = "id_zespolu",
            zespoły_w_albumach_id_albumu = "id_albumu",

            tab_Lokalizacje = "Lokalizacje",
            lokalizacje_id_lokalizacji = "id_lokalizacji",
            lokalizacje_nazwa_działu = "nazwa_dzialu",
            lokalizacje_symbol_półki = "symbol_polki",

            tab_Lokalizacje_Albumów = "Lokalizacje_Albumow",
            lokalizacje_albumów_id_lokalizacji = "Lokalizacje_id_lokalizacji",
            lokalizacje_albumów_id_albumu = "Albumy_id_albumu";




    }
}
