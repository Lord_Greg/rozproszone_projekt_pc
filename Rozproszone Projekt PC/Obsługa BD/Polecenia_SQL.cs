﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rozproszone_Projekt_PC.Obsługa_BD
{
    class Polecenia_SQL
    {

        public static string wczytajCałąTabele(string nazwaTabeli)
        {
            string polecenie;
            polecenie = "SELECT * FROM " + nazwaTabeli + ";";
            return polecenie;
        }



        public static string wczytajCałąTabele(string nazwaTabeli, string warunek)
        {
            string polecenie;
            polecenie = "SELECT * FROM " + nazwaTabeli + " WHERE " + warunek + ";";
            return polecenie;
        }



        public static string wczytajNPól(string nazwaTabeli, string[] nazwyPól)
        {
            string polecenie;
            polecenie = "SELECT ";

            polecenie += nazwyPól[0];
            for (int i = 1; i < nazwyPól.Length; i++)
            {
                polecenie += (", " + nazwyPól[i]);
            }

            polecenie += " FROM " + nazwaTabeli + ";";
            return polecenie;
        }



        public static string aktualizujNPól(string nazwaTabeli, string[] nazwyPól, string[] noweWartości, string warunek)
        {
            string polecenie;
            polecenie = "UPDATE " + nazwaTabeli + " SET " + nazwyPól[0] + "=" + '"' + noweWartości[0] + '"';
            for (int i = 1; i < nazwyPól.Length; i++)
            {
                polecenie += ", " + nazwyPól[i] + "=" + '"' + noweWartości[i] + '"';
            }
            polecenie += " WHERE " + warunek + ";";
            return polecenie;
        }

        public static string aktualizujNPól_Specjalne(string nazwaTabeli, string[] nazwyPól, string[] noweWartości, string warunek)
        {
            string polecenie;
            polecenie = "UPDATE " + nazwaTabeli + " SET " + nazwyPól[0] + "=" + noweWartości[0];
            for (int i = 1; i < nazwyPól.Length; i++)
            {
                polecenie += ", " + nazwyPól[i] + "=" + noweWartości[i];
            }
            polecenie += " WHERE " + warunek + ";";
            return polecenie;
        }


        public static string aktualizujWszystkieRekordy(string nazwaTabeli, string nazwaPola, string nowaWartość)
        {
            string polecenie;
            polecenie = "UPDATE " + nazwaTabeli + " SET " + nazwaPola + "=" + '"' + nowaWartość + '"';
            return polecenie;
        }


        public static string usuńRekordy(string nazwaTabeli, string[] nazwyPól, string[] wartości)
        {
            string polecenie;
            polecenie = "DELETE FROM " + nazwaTabeli + " WHERE " + nazwyPól[0] + "=" + '"' + wartości[0] + '"';
            for (int i = 1; i < nazwyPól.Length; i++)
            {
                polecenie += " AND " + nazwyPól[i] + "=" + '"' + wartości[i] + '"';
            }
            polecenie += ";";
            return polecenie;
        }
        

        public static string wstawWartości(string nazwaTabeli, string[] wartości)
        {
            string polecenie;
            polecenie = "INSERT INTO " + nazwaTabeli + " VALUES(" + '"' + wartości[0] + '"';
            for (int i = 1; i < wartości.Length; i++)
            {
                polecenie += ", " + '"' + wartości[i] + '"';
            }
            polecenie += ")";

            return polecenie;
        }


        public static string wstawWartości_1z3(string nazwaTabeli)
        {
            string polecenie;
            polecenie = "INSERT INTO " + nazwaTabeli + " VALUES(";
            
            return polecenie;
        }

        public static string wstawWartości_2z3_Zwykłe(string[] wartości, bool czyToPoczątek)
        {
            string polecenie = "";

            if (!czyToPoczątek) polecenie += ", ";

            polecenie += '"' + wartości[0] + '"';
            for (int i = 1; i < wartości.Length; i++)
            {
                polecenie += ", " + '"' + wartości[i] + '"';
            }

            return polecenie;
        }

        public static string wstawWartości_2z3_Specjalne(string[] wartości, bool czyToPoczątek)
        {
            for(int i=0; i<wartości.Length; i++)
            {
                if (wartości[i] == "" || wartości[i] == "0" || wartości[i] == "-1") wartości[i] = "null";
            }

            string polecenie = "";

            if (!czyToPoczątek) polecenie += ", ";

            polecenie += wartości[0];
            for (int i = 1; i < wartości.Length; i++)
            {
                polecenie += ", " + wartości[i];
            }

            return polecenie;
        }

        public static string wstawWartości_3z3()
        {
            string polecenie;
            polecenie = ")";

            return polecenie;
        }
        

    }
}
